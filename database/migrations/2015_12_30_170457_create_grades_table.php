<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->increments('id');

            $table->float('grade');
            $table->string('feedback');
            $table->unsignedInteger('evaluation_id');
            $table->unsignedInteger('professor_user_id')->nullable();
            $table->unsignedInteger('student_user_id');

            $table->timestamps();

            $table->foreign('evaluation_id')->references('id')->on('evaluations')->onDelete("cascade");
            $table->foreign('professor_user_id')->references('id')->on('users')->onDelete("set null");
            $table->foreign('student_user_id')->references('id')->on('users')->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grades');
    }
}
