<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_groups', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 70);
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('subject_activity_type_id');
            //$table->unsignedInteger('subject_id');
            $table->unsignedInteger('room_id')->nullable();
            $table->time('hour');

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('subject_activity_type_id')->references('id')->on('subject_activity_types')->onDelete('cascade');
            //$table->foreign('subject_id')->references('id')->on('subjects');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subject_groups');
    }
}
