<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 70);
            $table->unsignedInteger('faculty_id');
            $table->unsignedInteger('language_id')->nullable();

            $table->foreign('faculty_id')->references('id')->on('faculties')->onDelete("cascade");
            $table->foreign('language_id')->references('id')->on('languages')->onDelete("set null");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sections');
    }
}
