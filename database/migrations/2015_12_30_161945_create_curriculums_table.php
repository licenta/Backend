<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurriculumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculums', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('section_id');
            $table->unsignedInteger('first_semester_id');
            $table->unsignedInteger('last_semester_id');

            $table->foreign('section_id')->references('id')->on('sections')->onDelete("cascade");
            $table->foreign('first_semester_id')->references('id')->on('semesters')->onDelete("cascade");
            $table->foreign('last_semester_id')->references('id')->on('semesters')->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('curriculums');
    }
}
