<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 70);
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('first_name', 70);
            $table->string('last_name', 70);
            $table->unsignedInteger('entity_id')->nullable();
            $table->string('entity_type', 45)->nullable();

            $table->unique(['entity_id', 'entity_type']);

            $table->rememberToken();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
