<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->unsignedInteger('academic_year_id');
            $table->unsignedInteger('curriculum_id');

            $table->foreign('academic_year_id')->references('id')->on('academic_years')->onDelete("cascade");
            $table->foreign('curriculum_id')->references('id')->on('curriculums')->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('groups');
    }
}
