<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professors', function (Blueprint $table) {
            $table->increments('id');

            //$table->unsignedInteger('user_id');
            $table->string('professor_type', 70); //TODO as putea sa pun type enum, [asistent, doctorand, lector, profesor universitar]
            $table->unsignedInteger('department_id')->nullable();

            $table->timestamps();

            //$table->foreign('user_id')->references('id')->on('users');
            $table->foreign('department_id')->references('id')->on('departments')->onDelete("set null");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('professors');
    }
}
