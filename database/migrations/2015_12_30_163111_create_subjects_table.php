<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 120);
            $table->unsignedInteger('semester_id');
            $table->unsignedInteger('curriculum_id');
            $table->unsignedInteger('discipline_type_id')->nullable();
            $table->unsignedInteger('examination_type_id')->nullable();

            $table->timestamps();

            $table->foreign('semester_id')->references('id')->on('semesters')->onDelete("cascade");
            $table->foreign('curriculum_id')->references('id')->on('curriculums')->onDelete("cascade");
            $table->foreign('discipline_type_id')->references('id')->on('discipline_types')->onDelete("set null");
            $table->foreign('examination_type_id')->references('id')->on('examination_types')->onDelete("set null");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subjects');
    }
}
