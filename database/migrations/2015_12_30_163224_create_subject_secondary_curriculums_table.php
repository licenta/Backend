<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectSecondaryCurriculumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_secondary_curriculums', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('curriculum_id');
            $table->unsignedInteger('semester_id');
            $table->unsignedInteger('subject_id');

            $table->foreign('curriculum_id')->references('id')->on('curriculums')->onDelete("cascade");
            $table->foreign('semester_id')->references('id')->on('semesters')->onDelete("cascade");
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subject_secondary_curriculums');
    }
}
