<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectGroupUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_group_user', function (Blueprint $table) {
            $table->primary(['subject_group_id', 'user_id']);
            $table->unsignedInteger('subject_group_id');
            $table->unsignedInteger('user_id');

            $table->timestamps();

            $table->foreign('subject_group_id')->references('id')->on('subject_groups')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subject_group_user');
    }
}
