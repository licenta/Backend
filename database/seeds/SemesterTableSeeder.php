<?php

use Illuminate\Database\Seeder;
use App\Semester;

class SemesterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('semesters')->delete();

        $academic_years = \App\AcademicYear::all();
        foreach ($academic_years as $academic_year) {
            $academic_year->Semester()->saveMany([
                new Semester(['parity' => 1]),
                new Semester(['parity' => 2])
            ]);
        }
    }
}
