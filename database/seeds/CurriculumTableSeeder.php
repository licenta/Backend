<?php

use Illuminate\Database\Seeder;
use App\Section;
use App\Curriculum;
use App\AcademicYear;

class CurriculumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('curriculums')->delete();

        $academic_years = AcademicYear::with('Semester')->get()->sortBy('start_year');
        $sections = Section::all();
        foreach ($sections as $section) {
            Curriculum::create([
                'section_id' => $section->id,
                'first_semester_id' => $academic_years[0]->Semester->where('parity', 1)->first()->id,
                'last_semester_id' => $academic_years[2]->Semester->where('parity', 2)->first()->id
            ]);

            Curriculum::create([
                'section_id' => $section->id,
                'first_semester_id' => $academic_years[1]->Semester->where('parity', 1)->first()->id,
                'last_semester_id' => $academic_years[3]->Semester->where('parity', 2)->first()->id
            ]);

            Curriculum::create([
                'section_id' => $section->id,
                'first_semester_id' => $academic_years[2]->Semester->where('parity', 1)->first()->id,
                'last_semester_id' => $academic_years[4]->Semester->where('parity', 2)->first()->id
            ]);

            Curriculum::create([
                'section_id' => $section->id,
                'first_semester_id' => $academic_years[3]->Semester->where('parity', 1)->first()->id,
                'last_semester_id' => $academic_years[5]->Semester->where('parity', 2)->first()->id
            ]);

            Curriculum::create([
                'section_id' => $section->id,
                'first_semester_id' => $academic_years[4]->Semester->where('parity', 1)->first()->id,
                'last_semester_id' => $academic_years[6]->Semester->where('parity', 2)->first()->id
            ]);
        }
    }
}
