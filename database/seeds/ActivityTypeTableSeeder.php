<?php

use Illuminate\Database\Seeder;
use App\ActivityType;

class ActivityTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activity_types')->delete();

        ActivityType::create(['name' => 'curs']);
        ActivityType::create(['name' => 'laborator']);
        ActivityType::create(['name' => 'seminar']);
    }
}
