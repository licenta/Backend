<?php

use Illuminate\Database\Seeder;
use App\Department;
use App\Faculty;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->delete();

        $mate = Faculty::where('name', 'Facultatea de Matematică şi Informatică')->first();
        $fizica = Faculty::where('name', 'Facultatea de Fizică')->first();
        $chimie = Faculty::where('name', 'Facultatea de Chimie şi Inginerie Chimică')->first();
        $biologie = Faculty::where('name', 'Facultatea de Biologie şi Geologie')->first();
        $geografie = Faculty::where('name', 'Facultatea de Geografie')->first();
        $mediu = Faculty::where('name', 'Facultatea de Ştiinţa şi Ingineria Mediului')->first();
        $drept = Faculty::where('name', 'Facultatea de Drept')->first();
        $litere = Faculty::where('name', 'Facultatea de Litere')->first();
        $istore = Faculty::where('name', 'Facultatea de Istorie şi Filosofie')->first();

        $mate->Department()->saveMany([
            new Department(['name' => 'Departamentul de Informatica']),
            new Department(['name' => 'Departamentul de Matematica']),
            new Department(['name' => 'Departamentul de Matematica si Informatica al linie maghiare']),
        ]);

        $fizica->Department()->saveMany([
            new Department(['name' => 'Departamentul de Fizica Biomedicala, Teoretica si Spectroscopie Moleculara']),
            new Department(['name' => 'Departamentul de Fizica Starii Condensate si a Tehnologiilor Avansate']),
            new Department(['name' => 'Departamentul de Fizica al liniei maghiare']),
        ]);

        $chimie->Department()->saveMany([
            new Department(['name' => 'Departamentul de Chimie']),
            new Department(['name' => 'Departamentul de Inginerie Chimică']),
            new Department(['name' => 'Departamentul de Chimie şi Inginerie Chimică al Liniei Maghiare']),
        ]);

        $biologie->Department()->saveMany([
            new Department(['name' => 'Departamentul de  Biologie Moleculara si Biotehnologie']),
            new Department(['name' => 'Departamentul de Taxonomie şi Ecologie']),
            new Department(['name' => 'Departamentul de Geologie']),
            new Department(['name' => 'Departamentul de Biologie şi Ecologie al liniei maghiare']),
        ]);

        $geografie->Department()->saveMany([
            new Department(['name' => 'Departamentul de Geografie Regională şi Planificare Teritorială']),
            new Department(['name' => 'Departamentul de Geografie Umană şi Turism']),
            new Department(['name' => 'Departamentul de Geografie Fizică şi Tehnică']),
            new Department(['name' => 'Departamentul de Geografie al Liniei Maghiare']),
        ]);

        $mediu->Department()->saveMany([
            new Department(['name' => 'Departamentul de Stiinta Mediului']),
            new Department(['name' => 'Departamentul de Analiza si Ingineria Mediului']),
        ]);

        $drept->Department()->saveMany([
            new Department(['name' => 'Departamentul de drept public']),
            new Department(['name' => 'Departamentul de drept privat']),
        ]);

        $litere->Department()->saveMany([
            new Department(['name' => 'Departamentul de Limba Română şi Lingvistică Generală']),
            new Department(['name' => 'Departamentul de Literatură Română, Teoria Literaturii şi Etnologie']),
            new Department(['name' => 'Departamentul de Limbă, Cultură şi Civilizaţie Românească']),
            new Department(['name' => 'Departamentul de Limbă Maghiară şi Lingvistică Generală']),
            new Department(['name' => 'Departamentul de Literatură Maghiară']),
            new Department(['name' => 'Departamentul de Etnografie şi Antropologie Maghiară']),
            new Department(['name' => 'Departamentul de Limba şi Literatura Engleză']),
            new Department(['name' => 'Departamentul de Limbi şi Literaturi Romanice']),
            new Department(['name' => 'Departamentul de Limba şi Literatura Germană']),
            new Department(['name' => 'Departamentul de Filologie Slavă']),
            new Department(['name' => 'Departamentul de Limbi şi Literaturi Scandinave']),
            new Department(['name' => 'Departamentul de Filologie Clasică']),
            new Department(['name' => 'Departamentul de Limbi Moderne Aplicate']),
            new Department(['name' => 'Departamentul de Limbi Străine Specializate']),
            new Department(['name' => 'Departamentul de Literatură Comparată']),
            new Department(['name' => 'Departamentul de Limbi şi Literaturi Asiatice']),
        ]);

        $istore->Department()->saveMany([
            new Department(['name' => 'Departamentul de Istorie Antică şi Arheologie']),
            new Department(['name' => 'Departamentul de Istorie Medievală, Premodernă şi Istoria Artei']),
            new Department(['name' => 'Departamentul de Istorie Modernă, Arhivistică şi Etnologie']),
            new Department(['name' => 'Departamentul de Studii Internaţionale şi Istorie Contemporană']),
            new Department(['name' => 'Departamentul de Istorie al Liniei Maghiare']),
            new Department(['name' => 'Departamentul de Filosofie']),
            new Department(['name' => 'Departamentul de Filosofie al Liniei Maghiare']),
            new Department(['name' => 'Departamentul de Filosofie Premodernă şi Românească']),
        ]);
    }
}
