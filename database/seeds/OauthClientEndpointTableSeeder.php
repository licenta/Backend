<?php

use Illuminate\Database\Seeder;

class OauthClientEndpointTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_client_endpoints')->delete();

        $client_1 = DB::table('oauth_clients')->where('name', 'Client 1')->first();
        DB::table('oauth_client_endpoints')->insert(['client_id' => $client_1->id, 'redirect_uri' => config('app.url')]);

        $client_2 = DB::table('oauth_clients')->where('name', 'Client 2')->first();
        DB::table('oauth_client_endpoints')->insert(['client_id' => $client_2->id, 'redirect_uri' => 'http://licenta-front.dev:8000/client2']);

        $examiner = DB::table('oauth_clients')->where('name', 'Examiner')->first();
        DB::table('oauth_client_endpoints')->insert(['client_id' => $examiner->id, 'redirect_uri' => 'http://examiner.dev:8000/login_callback']);
    }
}
