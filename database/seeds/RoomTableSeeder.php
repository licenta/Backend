<?php

use Illuminate\Database\Seeder;
use App\Room;

class RoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->delete();

        $faker = Faker\Factory::create();
        for ($i = 1; $i < 11; $i++) {
            Room::create(['name' => 'Room ' . $i, 'location' => $faker->streetAddress, 'extra_info' => $faker->sentence]);
        }
    }
}
