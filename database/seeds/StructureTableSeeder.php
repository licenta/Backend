<?php

use Illuminate\Database\Seeder;

class StructureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('structures')->delete();
        $faker = \Faker\Factory::create();

        $semesters = \App\Semester::with('FirstCurriculum')->with('LastCurriculum')->with('AcademicYear')->get();
        foreach ($semesters as $semester) {
            $start_year = \Carbon\Carbon::parse($semester->AcademicYear->start_year . '-00-00');
            $end_year = \Carbon\Carbon::parse($semester->AcademicYear->end_year . '-00-00');
            $curriculums = $semester->FirstCurriculum->merge($semester->LastCurriculum);
            foreach ($curriculums as $curriculum) {
                $from_date = $faker->dateTimeBetween($start_year, strtotime($end_year . ' -' . rand(2, 4) . ' months'));
                $to_date = $faker->dateTimeBetween($from_date, $end_year);
                $semester->Structure()->create([
                    'description' => $faker->sentence,
                    'from_date' => $from_date,
                    'to_date' => $to_date,
                    'curriculum_id' => $curriculum->id
                ]);
            }
        }
    }
}
