<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LanguageTableSeeder::class);
        $this->call(FacultyTableSeeder::class);
        $this->call(SectionTableSeeder::class);
        $this->call(AcademicYearTableSeeder::class);
        $this->call(SemesterTableSeeder::class);
        $this->call(CurriculumTableSeeder::class);
        $this->call(StructureTableSeeder::class);
        $this->call(DisciplineTypeTableSeeder::class);
        $this->call(ExaminationTypeTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        $this->call(ActivityTypeTableSeeder::class);
        $this->call(SubjectTableSeeder::class);
        $this->call(RoomTableSeeder::class);
        $this->call(EvaluationTableSeeder::class);

        $this->call(RoleTableSeeder::class);
        $this->call(ProfessorTableSeeder::class);
        $this->call(StudentTableSeeder::class);
        $this->call(GroupTableSeeder::class);
        $this->call(UserTableSeeder::class);

        $this->call(SubjectActivityTypeTableSeeder::class);
        $this->call(SubjectGroupTableSeeder::class);
        $this->call(GradeTableSeeder::class);

        $this->call(OauthScopesTableSeeder::class);
        $this->call(OauthGrantTableSeeder::class);
        $this->call(OauthClientTableSeeder::class);
        $this->call(OauthClientEndpointTableSeeder::class);
    }
}
