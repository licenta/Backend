<?php

use Illuminate\Database\Seeder;

class OauthClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->delete();
        DB::table('oauth_client_scopes')->delete();

        $user = \App\User::where('name', 'Admin 1')->first();
        DB::table('oauth_clients')->insert(['id' => 1, 'secret' => '123', 'name' => 'Client 1', 'user_id' => $user->id]);
        DB::table('oauth_client_scopes')->insert(['client_id' => 1, 'scope_id' => 'basic_user_read']);
        DB::table('oauth_client_scopes')->insert(['client_id' => 1, 'scope_id' => 'advanced_user_read']);
        DB::table('oauth_client_scopes')->insert(['client_id' => 1, 'scope_id' => 'professor_write']);
        DB::table('oauth_client_scopes')->insert(['client_id' => 1, 'scope_id' => 'oauth_manager']);
        DB::table('oauth_client_scopes')->insert(['client_id' => 1, 'scope_id' => 'admin_write']);

        DB::table('oauth_client_grants')->insert(['client_id' => 1, 'grant_id' => 'password']);
        DB::table('oauth_client_grants')->insert(['client_id' => 1, 'grant_id' => 'refresh_token']);

        $user = \App\User::where('name', 'Admin 2')->first();
        DB::table('oauth_clients')->insert(['id' => 123456, 'secret' => '123456', 'name' => 'Examiner', 'user_id' => $user->id]);
        DB::table('oauth_client_scopes')->insert(['client_id' => 123456, 'scope_id' => 'basic_user_read']);
        DB::table('oauth_client_scopes')->insert(['client_id' => 123456, 'scope_id' => 'advanced_user_read']);

        DB::table('oauth_client_grants')->insert(['client_id' => 123456, 'grant_id' => 'authorization_code']);

        $user = \App\User::where('name', 'Student 1')->first();
        DB::table('oauth_clients')->insert(['id' => 2, 'secret' => '234', 'name' => 'Client 2', 'user_id' => $user->id]);
        DB::table('oauth_client_scopes')->insert(['client_id' => 2, 'scope_id' => 'basic_user_read']);
        DB::table('oauth_client_grants')->insert(['client_id' => 2, 'grant_id' => 'authorization_code']);

        DB::table('oauth_clients')->insert(['id' => 3, 'secret' => '345', 'name' => 'Client 3', 'user_id' => $user->id]);
        DB::table('oauth_client_grants')->insert(['client_id' => 3, 'grant_id' => 'authorization_code']);

    }
}
