<?php

use Illuminate\Database\Seeder;

class EvaluationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('evaluations')->delete();
        $faker = \Faker\Factory::create();

        $subjects = \App\Subject::all();
        $posible_names = ['lab 1', 'lab 2', 'lab 5', 'examen final', 'partial', 'seminar 6'];

        foreach ($subjects as $subject) {
            $subject->Evaluation()->create([
                'name' => $posible_names[array_rand($posible_names)],
                'code' => rand(0, 8),
                'uploadable_solution' => $faker->boolean(),
                'start_date' => $faker->dateTimeBetween(),
                'end_date' => $faker->dateTimeBetween()]);
        }
    }
}
