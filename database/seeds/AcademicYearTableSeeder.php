<?php

use Illuminate\Database\Seeder;
use App\AcademicYear;

class AcademicYearTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('academic_years')->delete();

        AcademicYear::create(['start_year' => 2012, 'end_year' => 2013]);
        AcademicYear::create(['start_year' => 2013, 'end_year' => 2014]);
        AcademicYear::create(['start_year' => 2014, 'end_year' => 2015]);
        AcademicYear::create(['start_year' => 2015, 'end_year' => 2016]);
        AcademicYear::create(['start_year' => 2016, 'end_year' => 2017]);
        AcademicYear::create(['start_year' => 2017, 'end_year' => 2018]);
        AcademicYear::create(['start_year' => 2018, 'end_year' => 2019]);
        AcademicYear::create(['start_year' => 2019, 'end_year' => 2020]);
    }
}
