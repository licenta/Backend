<?php

use Illuminate\Database\Seeder;

class SubjectGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subject_group_user')->delete();
        DB::table('subject_groups')->delete();

        $faker = \Faker\Factory::create();
        $professors = \App\Department::where('name', 'Departamentul de Informatica')->first()->Professor;
        $groups = \App\AcademicYear::where('start_year', 2016)->first()->Group->load('User');
        $rooms = \App\Room::all();
        $subject_activity_types = \App\SubjectActivityType::all();

        foreach ($subject_activity_types as $subject_activity_type) {
            if ($subject_activity_type->ActivityType->name == 'curs') {
                $subject_group = \App\SubjectGroup::create([
                    'name' => '---',
                    'user_id' => $professors->random(1)->User->first()->id,
                    'subject_activity_type_id' => $subject_activity_type->id,
                    'room_id' => $rooms->random(1)->id,
                    'hour' => $faker->time('H', '18:00:00').':0:0'
                ]);
                $subject_group->Student()->attach($groups->pluck('User')->flatten()->pluck('id')->toArray());
            } else {
                $selected_groups = [];
                for ($i = 0; $i < 3; $i++) {
                    $group = $groups->random(1);
                    while(in_array($group->id, $selected_groups)){
                        $group = $groups->random(1);
                    }
                    $selected_groups[] = $group->id;
                    $subject_group = \App\SubjectGroup::create([
                        'name' => $group->name,
                        'user_id' => $professors->random(1)->User->first()->id,
                        'subject_activity_type_id' => $subject_activity_type->id,
                        'room_id' => $rooms->random(1)->id,
                        'hour' => $faker->time('H', '18:00:00').':0:0'
                    ]);
                    $subject_group->Student()->attach($group->User->pluck('id')->toArray());
                }
            }
        }
    }
}
