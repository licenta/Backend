<?php

use Illuminate\Database\Seeder;
use App\Professor;
use App\User;

class ProfessorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('professors')->delete();

        $info_department = \App\Department::where('name', 'Departamentul de Informatica')->first();
        $professor_type = ['asistent', 'doctorand', 'lector', 'profesor universitar'];

        for ($i = 1; $i <= 5; $i++) {
            App\Professor::create([
                'professor_type' => $professor_type[array_rand($professor_type)],
                'department_id' => $info_department->id
            ]);
        }
    }
}
