<?php

use Illuminate\Database\Seeder;

class OauthGrantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_grants')->delete();

        DB::table('oauth_grants')->insert(['id' => 'authorization_code']);
        DB::table('oauth_grants')->insert(['id' => 'password']);
        DB::table('oauth_grants')->insert(['id' => 'refresh_token']);
    }
}
