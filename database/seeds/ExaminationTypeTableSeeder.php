<?php

use Illuminate\Database\Seeder;
use App\ExaminationType;

class ExaminationTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('examination_types')->delete();

        ExaminationType::create(['name' => 'scris']);
        ExaminationType::create(['name' => 'practic']);
        ExaminationType::create(['name' => 'laborator']);
        ExaminationType::create(['name' => 'proiect']);
    }
}
