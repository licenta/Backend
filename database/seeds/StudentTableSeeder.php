<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Student;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->delete();
        $faker = \Faker\Factory::create();

        for ($i = 1; $i <= 5; $i++) {
            $student = App\Student::create([
                'registration_number' => $faker->randomNumber(5),
            ]);
            $user = $student->User;
        }
    }
}
