<?php

use Illuminate\Database\Seeder;
use App\Faculty;

class FacultyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->delete();

        Faculty::insert([
            ['name' => 'Facultatea de Matematică şi Informatică'],
            ['name' => 'Facultatea de Fizică'],
            ['name' => 'Facultatea de Chimie şi Inginerie Chimică'],
            ['name' => 'Facultatea de Biologie şi Geologie'],
            ['name' => 'Facultatea de Geografie'],
            ['name' => 'Facultatea de Ştiinţa şi Ingineria Mediului'],
            ['name' => 'Facultatea de Drept'],
            ['name' => 'Facultatea de Litere'],
            ['name' => 'Facultatea de Istorie şi Filosofie']
        ]);
    }
}