<?php

use Illuminate\Database\Seeder;

class GradeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grades')->delete();

        $faker = \Faker\Factory::create();

        $subjects = \App\Subject::has('Evaluation')->has('SubjectActivityType')->with('Evaluation')->get();

        foreach ($subjects as $subject) {
            $subject_activity_type = $subject->SubjectActivityType;
            $professor = $subject_activity_type->load('SubjectGroup.Professor')->lists('SubjectGroup')->collapse()->lists('Professor');
            $student = $subject_activity_type->load('SubjectGroup.Student')->lists('SubjectGroup')->collapse()->lists('Student')->collapse();
            foreach ($subject->Evaluation as $evaluation) {
                $evaluation->Grade()->create([
                    'grade' => $faker->randomFloat(2, 1, 10),
                    //'upload_date' => $faker->dateTimeThisYear(),
                    'feedback' => $faker->text(),
                    'student_user_id' => $student->random(1)->id,
                    'professor_user_id' => $professor->random(1)->id
                ]);
            }
        }
    }
}
