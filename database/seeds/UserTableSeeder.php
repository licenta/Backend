<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $faker = \Faker\Factory::create();
        $admin_role = Role::where('name', 'admin')->first();
        $teacher_role = Role::where('name', 'teacher')->first();
        $student_role = Role::where('name', 'student')->first();
        $api_owner = Role::where('name', 'api_owner')->first();

        $group_2014 = \App\Group::whereHas('AcademicYear', function ($query) {
            return $query->where('start_year', 2014);
        })->whereHas('Curriculum.Section.Faculty', function ($query) {
            return $query->where('name', 'Facultatea de Matematică şi Informatică');
        })->get();

        $group_2015 = \App\Group::whereHas('AcademicYear', function ($query) {
            return $query->where('start_year', 2015);
        })->whereHas('Curriculum.Section.Faculty', function ($query) {
            return $query->where('name', 'Facultatea de Matematică şi Informatică');
        })->get();

        $group_2016 = \App\Group::whereHas('AcademicYear', function ($query) {
            return $query->where('start_year', 2016);
        })->whereHas('Curriculum.Section.Faculty', function ($query) {
            return $query->where('name', 'Facultatea de Matematică şi Informatică');
        })->get();

        $i = 1;
        $users[] = User::create([
            'name' => 'Admin ' . $i++,
            'email' => 'user1@example.com',
            'password' => Hash::make('123456'),
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName
        ]);
        $users[0]->Role()->attach([$admin_role->id, $api_owner->id]);

        $users[] = User::create([
            'name' => 'Admin ' . $i++,
            'email' => 'user2@example.com',
            'password' => Hash::make('123456'),
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName
        ]);
        $users[1]->Role()->attach($admin_role->id);

        // Teachers
        $teachers = \App\Professor::all();
        foreach ($teachers as $key => $teacher) {
            $user = User::create([
                'name' => 'Teacher ' . ($key + 1),
                'email' => 'user' . $i++ . '@example.com',
                'password' => Hash::make('123456'),
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'entity_id' => $teacher->id,
                'entity_type' => 'professor'
            ]);
            $user->Role()->attach($teacher_role->id);
        }

        $students = \App\Student::all();
        foreach ($students as $key => $student) {
            $user = User::create([
                'name' => 'Student ' . ($key + 1),
                'email' => 'user' . $i++ . '@example.com',
                'password' => Hash::make('123456'),
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'entity_id' => $student->id,
                'entity_type' => 'student'
            ]);
            $user->Group()->attach([$group_2014->random(1)->id, $group_2015->random(1)->id, $group_2016->random(1)->id]);
            $user->Role()->attach($student_role->id);
        }
        $user = User::where('name', 'Student 1')->first();
        $user->Role()->attach($api_owner->id);

        //$factory_users = factory(App\User::class, 10)->create();
        //$student->User()->attach($factory_users->pluck('id')->all());

        //$factory_users = factory(App\User::class, 10)->create();
        //$teacher->User()->attach($factory_users->pluck('id')->all());
    }
}
