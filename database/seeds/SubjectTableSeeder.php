<?php

use Illuminate\Database\Seeder;
use App\Subject;
use App\AcademicYear;

class SubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->delete();
        $examination_types = \App\ExaminationType::all();
        $discipline_types = \App\DisciplineType::all();

        $examination_scris = $examination_types->where('name', 'scris')->first();
        $examination_practic = $examination_types->where('name', 'practic')->first();

        $discipline_obligatorie = $discipline_types->where('name', 'obligatorie')->first();
        $discipline_optional = $discipline_types->where('name', 'optional')->first();
        $discipline_facultativ = $discipline_types->where('name', 'facultativ')->first();

        $romana = \App\Language::where('name', 'romana')->first();
        $semester_1_2015 = AcademicYear::where('start_year', 2015)->first()->Semester->where('parity', 1)->first();
        $semester_2_2015 = AcademicYear::where('start_year', 2015)->first()->Semester->where('parity', 2)->first();

        $semester_1_2016 = AcademicYear::where('start_year', 2016)->first()->Semester->where('parity', 1)->first();
        $semester_2_2016 = AcademicYear::where('start_year', 2016)->first()->Semester->where('parity', 2)->first();

        $semester_1_2017 = AcademicYear::where('start_year', 2017)->first()->Semester->where('parity', 1)->first();
        $semester_2_2017 = AcademicYear::where('start_year', 2017)->first()->Semester->where('parity', 2)->first();

        $curriculum_2014_2016 = \App\Faculty::where('name', 'Facultatea de Matematică şi Informatică')->first()
            ->Section->where('name', 'Informatică')->where('language_id', $romana->id)->first()
            ->Curriculum->where('first_semester_id', $semester_1_2015->id)->first();

        $data = [];
        $data[$semester_1_2015->id] = [
            ['name' => 'Analiza matematica', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Algebra', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Logica computationala', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Arhitectura sistemelor de calcul', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Fundamentele programarii', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Educatie fizica', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Psihologie educationala', 'discipline_type_id' => $discipline_facultativ->id, 'examination_type_id' => $examination_scris->id],
        ];
        $data[$semester_2_2015->id] = [
            ['name' => 'Programare orientata obiect', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Structuri de date si algoritmi', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Sisteme de operare', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Geometrie', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Sisteme dinamice', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Algoritmica grafelor', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Educatie fizica', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Metode avansate de rezolvare a problemelor de matematica si informatica', 'discipline_type_id' => $discipline_facultativ->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Fundamentele pedagogiei', 'discipline_type_id' => $discipline_facultativ->id, 'examination_type_id' => $examination_scris->id],
        ];
        $data[$semester_1_2016->id] = [
            ['name' => 'Metode avansate de programare', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Sisteme de operare distribuite', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Baze de date', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Programare logica si functionala', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Limba engleza', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Posibilitati si statistici', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Teoria si metodologia instruirii. Teoria si metodologia evaluarii', 'discipline_type_id' => $discipline_facultativ->id, 'examination_type_id' => $examination_scris->id],
        ];
        $data[$semester_2_2016->id] = [
            ['name' => 'Programare orientata pe aspecte', 'discipline_type_id' => $discipline_optional->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Practica in informatica', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_practic->id],
            ['name' => 'Inginerie sistemelor soft', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Sisteme de gestiune a bazelor de date', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Limba engleza', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Inteligenta artificiala', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Retele de calculatoare', 'discipline_type_id' => $discipline_facultativ->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Proiect individual', 'discipline_type_id' => $discipline_facultativ->id, 'examination_type_id' => $examination_scris->id],
        ];
        $data[$semester_1_2017->id] = [
            ['name' => 'Gestiunea proiectelor soft', 'discipline_type_id' => $discipline_optional->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Securitate software', 'discipline_type_id' => $discipline_optional->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Programare web', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Limbaje formale si tehnici de compilare', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Proiect colectiv', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
        ];
        $data[$semester_2_2017->id] = [
            ['name' => 'Medii de proiectare si dezvoltare', 'discipline_type_id' => $discipline_optional->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Verificarea si validarea sistemelor', 'discipline_type_id' => $discipline_optional->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Calcul numeric', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Elaborarea lucrarii de licenta', 'discipline_type_id' => $discipline_obligatorie->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Business intelligence', 'discipline_type_id' => $discipline_optional->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Aplicatii pentru dispozitive mobile', 'discipline_type_id' => $discipline_optional->id, 'examination_type_id' => $examination_scris->id],
            ['name' => 'Istoria informaticii', 'discipline_type_id' => $discipline_optional->id, 'examination_type_id' => $examination_scris->id],
        ];

        foreach($data as $semester_id => $subjects){
            foreach($subjects as $subject){
                Subject::create([
                    'name' => $subject['name'],
                    'semester_id' => $semester_id,
                    'curriculum_id' => $curriculum_2014_2016->id,
                    'discipline_type_id' => $subject['discipline_type_id'],
                    'examination_type_id' => $subject['examination_type_id']
                ]);
            }
        }
    }
}
