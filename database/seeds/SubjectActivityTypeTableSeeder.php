<?php

use Illuminate\Database\Seeder;
use App\Subject;
use App\ActivityType;
use App\SubjectActivityType;

class SubjectActivityTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subject_activity_types')->delete();

        $curs = ActivityType::where(['name' => 'curs'])->first();
        $lab = ActivityType::where(['name' => 'laborator'])->first();
        $seminar = ActivityType::where(['name' => 'seminar'])->first();

        Subject::where('name', 'Programare orientata obiect')->first()->SubjectActivityType()->saveMany([
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $curs->id]),
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $seminar->id]),
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $lab->id])
        ]);
        Subject::where('name', 'Structuri de date si algoritmi')->first()->SubjectActivityType()->saveMany([
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $curs->id]),
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $seminar->id]),
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $lab->id])
        ]);
        Subject::where('name', 'Sisteme de operare')->first()->SubjectActivityType()->saveMany([
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $curs->id]),
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $seminar->id]),
            new SubjectActivityType(['hour_count' => 60, 'activity_type_id' => $lab->id])
        ]);
        Subject::where('name', 'Geometrie')->first()->SubjectActivityType()->saveMany([
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $curs->id]),
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $seminar->id]),
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $lab->id])
        ]);
        Subject::where('name', 'Sisteme dinamice')->first()->SubjectActivityType()->saveMany([
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $curs->id]),
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $seminar->id]),
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $lab->id])
        ]);
        Subject::where('name', 'Algoritmica grafelor')->first()->SubjectActivityType()->saveMany([
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $curs->id]),
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $seminar->id])
        ]);
        Subject::where('name', 'Educatie fizica')->first()->SubjectActivityType()->saveMany([
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $curs->id])
        ]);
        Subject::where('name', 'Metode avansate de rezolvare a problemelor de matematica si informatica')->first()->SubjectActivityType()->saveMany([
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $curs->id]),
            new SubjectActivityType(['hour_count' => 120, 'activity_type_id' => $seminar->id])
        ]);
        Subject::where('name', 'Fundamentele pedagogiei')->first()->SubjectActivityType()->saveMany([
            new SubjectActivityType(['hour_count' => 60, 'activity_type_id' => $curs->id])
        ]);
    }
}
