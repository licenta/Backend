<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->delete();
        $curriculum = \App\Curriculum::whereHas('Section', function ($query) {
            return $query->where('name', 'Informatică')->whereHas('Language', function ($query) {
                return $query->where('name', 'romana');
            })->whereHas('Faculty', function ($query) {
                return $query->where('name', 'Facultatea de Matematică şi Informatică');
            });
        })->first();

        $academic_years = \App\AcademicYear::all();
        $j = 1;
        foreach ($academic_years as $academic_year) {
            for ($i = 1; $i < 4; $i++) {
                $curriculum->Group()->create(['name' => $j . '0' . $i, 'academic_year_id' => $academic_year->id]);
            }
            $j++;
        }
    }
}
