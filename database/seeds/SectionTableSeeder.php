<?php

use Illuminate\Database\Seeder;
use App\Faculty;
use App\Section;

class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->delete();

        $romana = \App\Language::where('name', 'romana')->first();
        $engleza = \App\Language::where('name', 'engleza')->first();
        $maghiara = \App\Language::where('name', 'maghiara')->first();
        $germana = \App\Language::where('name', 'germana')->first();

        $mate = Faculty::where('name', 'Facultatea de Matematică şi Informatică')->first();
        $fizica = Faculty::where('name', 'Facultatea de Fizică')->first();
        $chimie = Faculty::where('name', 'Facultatea de Chimie şi Inginerie Chimică')->first();
        $biologie = Faculty::where('name', 'Facultatea de Biologie şi Geologie')->first();
        $geografie = Faculty::where('name', 'Facultatea de Geografie')->first();
        $mediu = Faculty::where('name', 'Facultatea de Ştiinţa şi Ingineria Mediului')->first();
        $frept = Faculty::where('name', 'Facultatea de Drept')->first();
        $litere = Faculty::where('name', 'Facultatea de Litere')->first();
        $istore = Faculty::where('name', 'Facultatea de Istorie şi Filosofie')->first();

        $mate->Section()->saveMany([
            // Domeniul matematica, nivel licenta
            new Section(['name' => 'Matematică', 'language_id' => $romana->id]),
            new Section(['name' => 'Matematică', 'language_id' => $maghiara->id]),
            new Section(['name' => 'Matematica informatica', 'language_id' => $romana->id]),
            new Section(['name' => 'Matematica informatica', 'language_id' => $maghiara->id]),

            // Domeniul informatica, nivel licenta
            new Section(['name' => 'Informatică', 'language_id' => $romana->id]),
            new Section(['name' => 'Informatică', 'language_id' => $maghiara->id]),
            new Section(['name' => 'Informatică', 'language_id' => $engleza->id]),
            new Section(['name' => 'Informatică', 'language_id' => $germana->id]),

            // Domeniul matematica, nivel master
            new Section(['name' => 'Matematici avansate', 'language_id' => $engleza->id]),
            new Section(['name' => 'Matematică didactică', 'language_id' => $romana->id]),
            new Section(['name' => 'Matematică didactică', 'language_id' => $maghiara->id]),
            new Section(['name' => 'Matematică computaţională', 'language_id' => $maghiara->id]),

            // Domeniul informatica, nivel master
            new Section(['name' => 'Baze de date', 'language_id' => $romana->id]),
            new Section(['name' => 'Sisteme distribuite în Internet', 'language_id' => $romana->id]),
            new Section(['name' => 'Inginerie software', 'language_id' => $engleza->id]),
            new Section(['name' => 'Inteligenţă computaţională aplicată', 'language_id' => $engleza->id]),
            new Section(['name' => 'Proiectarea şi dezvoltarea aplicaţiilor enterprise', 'language_id' => $maghiara->id]),
            new Section(['name' => 'Calcul de înaltă performanţă şi analiza volumelor mari de date', 'language_id' => $engleza->id]),
            new Section(['name' => 'Informatică didactică', 'language_id' => $romana->id]),
            new Section(['name' => 'Informatică didactică', 'language_id' => $maghiara->id]),
            new Section(['name' => 'Analiza datelor şi modelare', 'language_id' => $maghiara->id]),
        ]);
    }
}
