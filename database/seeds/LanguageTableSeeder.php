<?php

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->delete();

        \App\Language::create(['name' => 'romana']);
        \App\Language::create(['name' => 'engleza']);
        \App\Language::create(['name' => 'maghiara']);
        \App\Language::create(['name' => 'germana']);
    }
}
