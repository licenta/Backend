<?php

use Illuminate\Database\Seeder;
use App\DisciplineType;

class DisciplineTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discipline_types')->delete();

        DisciplineType::create(['name' => 'obligatorie']);
        DisciplineType::create(['name' => 'optional']);
        DisciplineType::create(['name' => 'facultativ']);
    }
}
