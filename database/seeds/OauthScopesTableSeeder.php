<?php

use Illuminate\Database\Seeder;

class OauthScopesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_scopes')->delete();

        DB::table('oauth_scopes')->insert(['id' => 'basic_user_read', 'description' => 'Read basic information about authenticated user.']);
        DB::table('oauth_scopes')->insert(['id' => 'advanced_user_read', 'description' => 'Read advance information about authenticated user such attended subjects, grades and groups.']);
        DB::table('oauth_scopes')->insert(['id' => 'professor_write', 'description' => 'Write permission for evaluation in grades.']);
        DB::table('oauth_scopes')->insert(['id' => 'oauth_manager', 'description' => 'Read and write persmissions for oauth actions.']);
        DB::table('oauth_scopes')->insert(['id' => 'admin_write', 'description' => 'Sudo for most important actions.']);
    }
}
