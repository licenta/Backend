<?php

namespace Task;

use Mage\Task\AbstractTask;

class DatabaseMigrateRem extends AbstractTask {
    public function getName()
    {
        return 'php artisan migrate:refresh';
    }

    public function run()
    {
        return $this->runCommandRemote('php artisan migrate:refresh --force');
    }
}