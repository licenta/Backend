<?php

namespace Task;

use Mage\Task\AbstractTask;

class PermissionChmod extends AbstractTask {
    public function getName()
    {
        return 'chmod -R 777 bootstrap/cache storage';
    }

    public function run()
    {
        return $this->runCommandRemote('chmod -R 777 bootstrap/cache storage');
    }
}