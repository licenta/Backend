<?php

namespace Task;

use Mage\Task\AbstractTask;

class OptimizeRem extends AbstractTask {
    public function getName()
    {
        return 'php artisan optimize';
    }

    public function run()
    {
        return $this->runCommandRemote('php artisan optimize');
    }
}