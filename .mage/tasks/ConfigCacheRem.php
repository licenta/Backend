<?php

namespace Task;

use Mage\Task\AbstractTask;

class ConfigCacheRem extends AbstractTask {
    public function getName()
    {
        return 'php artisan config:cache';
    }

    public function run()
    {
        return $this->runCommandRemote('php artisan config:cache');
    }
}