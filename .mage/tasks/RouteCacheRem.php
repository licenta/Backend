<?php

namespace Task;

use Mage\Task\AbstractTask;

class RouteCacheRem extends AbstractTask {
    public function getName()
    {
        return 'php artisan route:cache';
    }

    public function run()
    {
        return $this->runCommandRemote('php artisan route:cache');
    }
}