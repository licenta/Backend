<?php

namespace Task;

use Mage\Task\AbstractTask;

class CacheClearRem extends AbstractTask {
    public function getName()
    {
        return 'php artisan cache:clear';
    }

    public function run()
    {
       return $this->runCommandRemote('php artisan cache:clear');
    }
}