<?php

namespace Task;

use Mage\Task\AbstractTask;

class KeyGenerate extends AbstractTask {
    public function getName()
    {
        return 'php artisan key:generate';
    }

    public function run()
    {
        return $this->runCommandRemote('php artisan key:generate');
    }
}