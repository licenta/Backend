<?php

namespace Task;

use Mage\Task\AbstractTask;

class CopyEnv extends AbstractTask {
    public function getName()
    {
        return 'cp ../.env ./';
    }

    public function run()
    {
        return $this->runCommandRemote('cp ../.env ./');
    }
}