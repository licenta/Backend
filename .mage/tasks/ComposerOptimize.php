<?php

namespace Task;

use Mage\Task\AbstractTask;

class ComposerOptimize extends AbstractTask {
    public function getName()
    {
        return 'composer dumpautoload --optimize';
    }

    public function run()
    {
        return $this->runCommandLocal('composer dumpautoload --optimize');
    }
}