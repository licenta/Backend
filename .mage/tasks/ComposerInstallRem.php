<?php

namespace Task;

use Mage\Task\AbstractTask;

class ComposerInstallRem extends AbstractTask {
    public function getName()
    {
        return 'php ../../composer.phar install';
    }

    public function run()
    {
        return $this->runCommandRemote('php ../../composer.phar install');
    }
}