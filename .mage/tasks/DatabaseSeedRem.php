<?php

namespace Task;

use Mage\Task\AbstractTask;

class DatabaseSeedRem extends AbstractTask {
    public function getName()
    {
        return 'php artisan db:seed';
    }

    public function run()
    {
        return $this->runCommandRemote('php artisan db:seed --force');
    }
}