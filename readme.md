## Backend for licenta

php with laravel, restful API based app

### config
All necessary config is in env.example. Just copy env.example to .env

### Docker

```
$ docker-compose up
```
- start backend (redis, php, nginx)

```
$ docker-compose down
```
- stop backend


### Setting up this beauty!

```
$ cp .env.example .env
```
- copy .env.example to .env (don't forget to edit this file according to your needs)

```
$ composer install
```
- install all dependencies for laravel and PHP

```
$ php artisan key:generate
```
- generate a new key for your application

```
$ php artisan migrate:refresh --seed
```
- migrate the database

```
$ npm install
```
- install dependencies from package.json

```
$ php artisan
```
- for a list of all available commands


### Database ###

```
$ php artisan migrate
```
- migrate the database

```
$ php artisan migrate:reset
```
- reset all migrations

```
$ php artisan db:seed
```
- seed the database with dummy data


### Deploy to production ###
```
$ vendor/bin/mage deploy to:production
```
- deploy application to production server

### Start real time server ###

```
$ node socket.js
```
- start node server

```
$ redis-server --port 3001
```
- start redis

```
$ redis-cli monitor
```
- start monitor utility for intercepting messages