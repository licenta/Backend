"use strict";

const app = require('http').createServer(handler)
const io = require('socket.io')(app)
const auth = require('socketio-auth')
require('dotenv').load();
const Redis = require('ioredis');
const redis = new Redis(process.env.REDIS_PORT);
const request = require('request')

app.listen(process.env.SOCKETIO_PORT, function () {
    console.log('Server starting on port : ' + process.env.SOCKETIO_PORT);
});

function handler(req, res) {
    res.writeHead(200);
    res.end('');
}

auth(io, {
    authenticate: function (socket, data, callback) {
        const token = data.token;

        return request.get('http://localhost:8000/api/v1/me', {
            headers: {
                Authorization: 'Bearer ' + token
            }
        }, function (error, r, b) {
            if(r.statusCode == 404){
                return callback(new Error("Something went wrong"));
            }
            const body = JSON.parse(b)
            socket.token = token;
            socket.user_id = body.id
            if (body.id) {
                return callback(null, true);
            } else {
                return callback(new Error("User not found"));
            }
        })
    },
    postAuthenticate: function (socket, data) {
        console.log(`user ${socket.user_id} connected`)
        const redis_handler = function (subscribed, channel, message) {
            if (channel == 'user.' + socket.user_id) {
                message = JSON.parse(message);
                console.log('write to ' + channel + ' channel');
                io.emit(channel + ':' + message.event, message.data);
            }
        };

        redis.on("pmessage", redis_handler);

        socket.on('disconnect', function () {
            console.log('user ' + socket.user_id + ' disconnected');
            redis.removeListener('pmessage', redis_handler);
            //redis.quit()
            socket.disconnect();
        });
    },
    timeout: 5000
});

redis.psubscribe('*', function (err, count) {
    console.log('psubscribe');
});