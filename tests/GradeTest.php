<?php

require('vendor/autoload.php');

class GradeTest extends PHPUnit_Framework_TestCase
{
    protected $client;
    protected $acces_token = "Bearer ";

    protected function setUp()
    {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => 'http://licenta-back.dev/api/v1/'
        ]);

        $response = $this->client->post('/oauth/access_token', [
            'json' => [
                'grant_type' => "password",
                'client_id' => "1",
                'client_secret' => "123",
                'username' => "user8@example.com",
                'password' => "123456",
                'scope' => "advanced_user_read,admin_write,basic_user_read,oauth_manager,professor_write"
            ]
        ]);

        $data = json_decode($response->getBody(), true);
        $this->acces_token .= $data['access_token']['access_token'];
    }

    public function testGet_Grades()
    {
        $response = $this->client->request('GET', 'user/grade', [
            'headers' => [
                'User-Agent' => 'testing/1.0',
                'Accept' => 'application/json',
                'authorization' => $this->acces_token
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(), true);

        $this->assertNotEmpty($data['grade']);
        $this->assertNotEmpty($data['grade']);
////        $this->assertEquals("Margarita", $data['first_name']);
////        $this->assertEquals("Wunsch", $data['last_name']);
//        $this->assertEquals(null, $data['entity_id']);
//        $this->assertEquals(null, $data['entity_type']);
//        $this->assertNotEmpty($data['access_token']['access_token']);
//        $this->assertEquals("Bearer", $data['access_token']['token_type']);
////        $this->assertEquals("21600", $data['access_token']['expires_in']);
//        $this->assertNotEmpty($data['access_token']['refresh_token']);

    }
    public function testGet_Grades_With_Student()
    {
        $url = 'user/grade?with=Student';
        $response = $this->client->request('GET', $url, [
            'headers' => [
                'User-Agent' => 'testing/1.0',
                'Accept' => 'application/json',
                'authorization' => $this->acces_token
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(), true);

        $this->assertNotEmpty($data['grade']);

        if (isset($data['grade']['data'][0]['student'])) {
            $this->assertNotEmpty($data['grade']['data'][0]['student']);
        }else{
            $this->fail('There is no student on the reponse from the request ' . $url);
        }
    }
    public function testGet_Grades_With_Professor()
    {
        $url = 'user/grade?with=Professor';
        $response = $this->client->request('GET', $url, [
            'headers' => [
                'User-Agent' => 'testing/1.0',
                'Accept' => 'application/json',
                'authorization' => $this->acces_token
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(), true);

        $this->assertNotEmpty($data['grade']);

        if (isset($data['grade']['data'][0]['professor'])) {
            $this->assertNotEmpty($data['grade']['data'][0]['professor']);
        }else{
            $this->fail('There is no professor on the reponse from the request ' . $url);
        }
    }
    public function testGet_Grades_With_Evaluation()
    {
        $url = 'user/grade?with=Evaluation';
        $response = $this->client->request('GET', $url, [
            'headers' => [
                'User-Agent' => 'testing/1.0',
                'Accept' => 'application/json',
                'authorization' => $this->acces_token
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(), true);

        $this->assertNotEmpty($data['grade']);

        if (isset($data['grade']['data'][0]['evaluation'])) {
            $this->assertNotEmpty($data['grade']['data'][0]['evaluation']);
        }else{
            $this->fail('There is no evaluation on the reponse from the request ' . $url);
        }
    }
    public function testGet_Grades_With_ALL()
    {
        $url = 'user/grade?with[]=Evaluation&with[]=Professor&with[]=Student';
        $response = $this->client->request('GET', $url, [
            'headers' => [
                'User-Agent' => 'testing/1.0',
                'Accept' => 'application/json',
                'authorization' => $this->acces_token
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(), true);

        $this->assertNotEmpty($data['grade']);

        if (isset($data['grade']['data'][0]['evaluation'])) {
            $this->assertNotEmpty($data['grade']['data'][0]['evaluation']);
        }else{
            $this->fail('There is no evaluation on the reponse from the request ' . $url);
        }
        if (isset($data['grade']['data'][0]['professor'])) {
            $this->assertNotEmpty($data['grade']['data'][0]['professor']);
        }else{
            $this->fail('There is no profesdsor on the reponse from the request ' . $url);
        }
        if (isset($data['grade']['data'][0]['student'])) {
            $this->assertNotEmpty($data['grade']['data'][0]['student']);
        }else{
            $this->fail('There is no student on the reponse from the request ' . $url);
        }
    }
}
