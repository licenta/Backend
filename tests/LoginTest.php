<?php

require('vendor/autoload.php');

class LoginTest extends PHPUnit_Framework_TestCase
{
    protected $client;

    protected function setUp()
    {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => 'http://licenta-back.dev/'
        ]);
    }

    public function testPost_Login_Oauth_Password()
    {
        $response = $this->client->post('/oauth/access_token', [
            'json' => [
                'grant_type' => "password",
                'client_id' => "1",
                'client_secret' => "123",
                'username' => "user1@example.com",
                'password' => "123456",
                'scope' => "advanced_user_read,admin_write,basic_user_read,oauth_manager,professor_write"
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getBody(), true);

        $this->assertEquals("Admin 1", $data['name']);
//        $this->assertEquals("Margarita", $data['first_name']);
//        $this->assertEquals("Wunsch", $data['last_name']);
        $this->assertEquals(null, $data['entity_id']);
        $this->assertEquals(null, $data['entity_type']);
        $this->assertNotEmpty($data['access_token']['access_token']);
        $this->assertEquals("Bearer", $data['access_token']['token_type']);
//        $this->assertEquals("21600", $data['access_token']['expires_in']);
        $this->assertNotEmpty($data['access_token']['refresh_token']);

    }

    public function testPost_Login_Oauth_Password_With_Role()
    {
        $url = '/oauth/access_token?with=Role';
        $response = $this->client->post($url, [
            'json' => [
                'grant_type' => "password",
                'client_id' => "1",
                'client_secret' => "123",
                'username' => "user2@example.com",
                'password' => "123456",
                'scope' => "advanced_user_read,admin_write,basic_user_read,oauth_manager,professor_write"
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getBody(), true);

        $this->assertEquals("Admin 2", $data['name']);
        $this->assertEquals("user2@example.com", $data['email']);
//        $this->assertEquals("Donnie", $data['first_name']);
//        $this->assertEquals("Morar", $data['last_name']);
        $this->assertEquals(null, $data['entity_id']);
        $this->assertEquals(null, $data['entity_type']);

        if (isset($data['role'])) {
            $this->assertEquals("1", $data['role'][0]['id']);
            $this->assertEquals("admin", $data['role'][0]['name']);
        }else{
            $this->fail('There is no role on the reponse from the request ' . $url);
        }
    }
}
