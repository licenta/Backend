<?php

namespace App;

use Jedrzej\Pimpable\PimpableTrait;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

trait ModelTrait
{
    use PimpableTrait;

    /**
     * Determine what attributes can not be used for search in request.
     *
     */
    public function getNotSearchableAttributes()
    {
        $base = ['page', 'access_token', 'client_id', 'client_secret', 'token', 'sort', 'with'];
        $except = [
            [
                'routes' => ['api.v1.user.grade.index', 'api.v1.user.group.index', 'api.v1.user.subject-group.index'],
                'except' => 'user_id'
            ],
            [
                'routes' => ['api.v1.user.subject-group.index'],
                'except' => 'semester_id'
            ],
        ];
        $route_name = \Request::route()->getName();
        foreach ($except as $except_item) {
            if (in_array($route_name, $except_item['routes'])) {
                $base[] = $except_item['except'];
            }
        }
        return $base;
    }

    /**
     * Allowed relations for with query
     * @return array
     */
    protected function getWithableRelations()
    {
        $final_scopes = ['AcademicYear', 'Semester', 'ActivityType', 'Language', 'ExaminationType', 'DisciplineType', 'Faculty', 'Department', 'Section'];

        if (request()->has('with') && auth()->check()) {
            //TODO add this in config
            $define_scopes = [
                'basic_user_read' => ['FirstSemester', 'LastSemester', 'User', 'Entity', 'Role'],
                'advanced_user_read' => ['Structure', 'Group', 'FirstCurriculum', 'LastCurriculum', 'Curriculum', 'Subject', 'SubjectActivityType', 'SubjectGroup', 'SubjectActivityType.SubjectGroup', 'SubjectActivityType.Subject', 'Room', 'Evaluation', 'Grade', 'Student', 'Professor'],
                'admin_write' => ['User'],
                'oauth_manager' => ['OauthClientEndpoint', 'OauthScope', 'User'],
            ];

            $auth_scopes = Authorizer::getChecker()->getAccessToken()->getSession()->getScopes();

            $keys = array_keys($auth_scopes);
            $scopes = array_only($define_scopes, $keys);

            $final_scopes = array_merge($final_scopes, array_flatten($scopes));
        }
        return $final_scopes;
    }
}