<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectSecondaryCurriculum extends Model
{
    public function Curriculum()
    {
        return $this->belongsTo(Curriculum::class);
    }

    public function Semester()
    {
        return $this->belongsTo(Semester::class);
    }

    public function Subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
