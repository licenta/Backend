<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectGroup extends Model
{
    use ModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'user_id', 'room_id', 'hour'];

    public function Room()
    {
        return $this->belongsTo(Room::class);
    }

    public function Student()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function Professor()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function SubjectActivityType()
    {
        return $this->belongsTo(SubjectActivityType::class);
    }
}
