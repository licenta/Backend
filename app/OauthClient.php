<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OauthClient extends Model
{
    use ModelTrait;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['secret'];

    /**
     * Create a new Client with generated id and secret
     * @param int $lenght
     * @return static
     */
    public function createTokens($lenght = 10)
    {
        $this->id = uniqid();
        $this->secret = bin2hex(random_bytes($lenght));
    }

    public function OauthClientEndpoint()
    {
        return $this->hasMany(OauthClientEndpoint::class, 'client_id');
    }

    public function OauthSession()
    {
        return $this->hasMany(OauthSession::class, 'client_id');
    }

    public function OauthScope()
    {
        return $this->belongsToMany(OauthScope::class, 'oauth_client_scopes', 'client_id', 'scope_id');
    }

    public function OauthGrant()
    {
        return $this->belongsToMany(OauthGrant::class, 'oauth_client_grants', 'client_id', 'grant_id');
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
