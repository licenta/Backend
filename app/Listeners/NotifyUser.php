<?php

namespace App\Listeners;

use App\Events\NotifyUserEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddGradeEvent  $event
     * @return void
     */
    public function handle(NotifyUserEvent $event)
    {
        //
    }
}
