<?php

namespace App;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use ModelTrait;
    use CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'first_name', 'last_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasRole($roles)
    {
        $check = Role::whereIn('name', $roles)->whereHas('User', function ($query) {
            return $query->where('users.id', $this->id);
        })->count();
        if ($check > 0) {
            return true;
        }
        return false;
    }

    /**
     * Get all of the owning entity models.
     */
    public function entity()
    {
        return $this->morphTo();
    }

    public function Role()
    {
        return $this->belongsToMany(Role::class);
    }

    public function OauthClient()
    {
        return $this->hasMany(OauthClient::class);
    }

    public function OauthSession()
    {
        return $this->hasMany(OauthSession::class, 'owner_id');
    }

    public function Group()
    {
        return $this->belongsToMany(Group::class);
    }

    public function Grade()
    {
        if ($this->hasRole(['student'])) {
            return $this->hasMany(Grade::class, 'student_user_id');
        }
        if ($this->hasRole(['teacher'])) {
            return $this->hasMany(Grade::class, 'professor_user_id');
        }
    }

    public function SubjectGroup()
    {
        if ($this->hasRole(['student'])) {
            return $this->belongsToMany(SubjectGroup::class)->withTimestamps();
        }
        if ($this->hasRole(['teacher'])) {
            return $this->hasMany(SubjectGroup::class);
        }
    }

    /**
     * Get all subscribed Subjects for user.
     * @param $select
     */
    public function Subject($select = '*')
    {
        if ($this->hasRole(['admin'])) {
            return;
        }

        // punem in $subject toate subject-urile la care invata un student
        // punem si pimp pentru a incarca relatii auxiliare
        // in pimp scoatem din filter user_id pentru ca $subject nu are coloana user_id
        $request_except_user = request()->except(['user_id', 'with', 'sort']);
        $filter_user_id = count($request_except_user) == 0 ? [''] : $request_except_user;
        $this->SubjectGroup->load(['SubjectActivityType.Subject' => function ($query) use (&$subject, $select, $filter_user_id) {
            $subject = $query->select($select)->pimp($filter_user_id)->get()->unique();
        }]);
        return $subject;
    }
}
