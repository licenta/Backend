<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    use ModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['parity'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function AcademicYear()
    {
        return $this->belongsTo(AcademicYear::class);
    }

    public function FirstCurriculum()
    {
        return $this->hasMany(Curriculum::class, 'first_semester_id');
    }

    public function LastCurriculum()
    {
        return $this->hasMany(Curriculum::class, 'last_semester_id');
    }

    public function Subject()
    {
        return $this->hasMany(Subject::class);
    }

    public function SubjectSecondaryCurriculum()
    {
        return $this->hasMany(SubjectSecondaryCurriculum::class);
    }

    public function Structure()
    {
        return $this->hasMany(Structure::class);
    }
}
