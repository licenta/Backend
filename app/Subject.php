<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use ModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'discipline_type_id', 'examination_type_id'];

    public function Semester()
    {
        return $this->belongsTo(Semester::class);
    }

    public function Curriculum()
    {
        return $this->belongsTo(Curriculum::class);
    }

    public function DisciplineType()
    {
        return $this->belongsTo(DisciplineType::class);
    }

    public function ExaminationType()
    {
        return $this->belongsTo(ExaminationType::class);
    }

    public function SubjectActivityType()
    {
        return $this->hasMany(SubjectActivityType::class);
    }

    public function SubjectGroup()
    {
        return $this->hasMany(SubjectGroup::class);
    }

    public function Evaluation()
    {
        return $this->hasMany(Evaluation::class);
    }

    public function SubjectSecondaryCurriculum()
    {
        return $this->hasMany(SubjectSecondaryCurriculum::class);
    }

}
