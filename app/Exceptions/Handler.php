<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        /*if(is_null($request->route())){
            return response()->json(['error' => 'Page not found'], 404);
        }*/

        if ($e instanceof FatalErrorException && !env('APP_DEBUG') && ($request->ajax() || $request->wantsJson())) {
            return response()->json(['error' => 'Whoops, looks like something went wrong'], 400);
        }

        if ($e instanceof \League\OAuth2\Server\Exception\OAuthException) {
            $data = [
                'error' => $e->errorType,
                'error_description' => $e->getMessage(),
            ];
            return response()->json($data, $e->httpStatusCode, $e->getHttpHeaders());
        }

        if (env('APP_DEBUG') && ($request->ajax() || $request->wantsJson())) {
            $status = 400;

            // If this exception is an instance of HttpException
            if ($this->isHttpException($e)) {
                // Grab the HTTP status code from the Exception
                $status = $e->getStatusCode();
            }
            return response()->json(['error' => $e->getMessage()], $status);
        }

        return parent::render($request, $e);
    }
}
