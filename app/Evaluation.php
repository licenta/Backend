<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    use ModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code', 'uploadable_solution', 'start_date', 'end_date'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['start_date', 'end_date'];

    public function Subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function Grade()
    {
        return $this->hasMany(Grade::class);
    }
}
