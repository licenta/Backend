<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OauthClientEndpoint extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['redirect_uri'];

    public function OauthClient()
    {
        return $this->belongsTo(OauthClient::class, 'client_id');
    }
}
