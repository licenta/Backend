<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectActivityType extends Model
{
    use ModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['hour_count', 'activity_type_id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function ActivityType()
    {
        return $this->belongsTo(ActivityType::class);
    }

    public function Subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function SubjectGroup()
    {
        return $this->hasMany(SubjectGroup::class);
    }

}
