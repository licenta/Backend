<?php

namespace App\Policies;

use App\Subject;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubjectPolicy
{
    use HandlesAuthorization;

    /**
     * Check if a user is subscribed to a particular subject.
     *
     * @return void
     */
    public function user_subscribed_to_subject(User $user, Subject $subject)
    {
        $role = $user->Role->whereIn('name', ['student', 'teacher']);
        if($role->count() == 0){
            return false;
        }

        $entity = $role->first()->name == 'student' ? 'Student' : 'Professor';
        $subject = Subject::whereHas("SubjectActivityType.SubjectGroup.{$entity}", function ($query) use ($user) {
            return $query->where('id', $user->id);
        })->find($subject->id);

        if ($subject == null) {
            return false;
        }
        return true;
    }
}
