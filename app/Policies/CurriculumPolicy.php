<?php

namespace App\Policies;

use App\Curriculum;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CurriculumPolicy
{
    use HandlesAuthorization;

    /**
     * Check if a user is subscribed to a particular curriculum from subjects.
     *
     * @return void
     */
    public function user_subscribed_to_curriculum_from_subjects(User $user, Curriculum $curriculum)
    {
        $role = $user->Role->whereIn('name', ['student', 'teacher']);
        if($role->count() == 0){
            return false;
        }

        $entity = $role->first()->name == 'student' ? 'Student' : 'Professor';
        $curriculum = Curriculum::whereHas("Subject.SubjectActivityType.SubjectGroup.{$entity}", function ($query) use ($user) {
            return $query->where('id', $user->id);
        })->find($curriculum->id);

        if ($curriculum == null) {
            return false;
        }
        return true;
    }

    /**
     * Check if a user is subscribed to a particular curriculum from groups.
     *
     * @return void
     */
    public function user_subscribed_to_curriculum_from_groups(User $user, Curriculum $curriculum)
    {
        if (!$user->hasRole(['student'])) {
            return false;
        }

        $curriculum = Curriculum::whereHas("Group.User", function ($query) use ($user) {
            return $query->where('id', $user->id);
        })->find($curriculum->id);

        if ($curriculum == null) {
            return false;
        }
        return true;
    }
}
