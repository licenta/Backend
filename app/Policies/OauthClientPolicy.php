<?php

namespace App\Policies;

use App\OauthClient;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OauthClientPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function user_owns_oauth_client(User $user, OauthClient $oauth_client)
    {
        if ($user->hasRole(['admin']) || $user->id == $oauth_client->user_id) {
            return true;
        }
        return false;
    }
}
