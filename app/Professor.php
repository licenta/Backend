<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['professor_type', 'department_id'];

    public function User()
    {
        return $this->morphMany(User::class, 'entity');
    }

    public function Department()
    {
        return $this->belongsTo(Department::class);
    }
}
