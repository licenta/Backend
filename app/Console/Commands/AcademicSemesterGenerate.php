<?php

namespace App\Console\Commands;

use App\AcademicYear;
use Illuminate\Console\Command;

class AcademicSemesterGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:academic-semester {x_times=1 : How many academic years to generate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate academic years and semesters for the next x years';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!is_numeric($this->argument('x_times'))) {
            $this->error('x_times must be integer');
            return;
        }
        $academic_max_end_year = AcademicYear::get()->max('end_year');
        for ($i = 1; $i <= $this->argument('x_times'); $i++) {
            $new_academic = AcademicYear::create(['start_year' => $academic_max_end_year, 'end_year' => $academic_max_end_year = $academic_max_end_year + 1]);
            $this->info('New academic year created: start_year:' . $new_academic->start_year . ', end_year:' . $new_academic->end_year);
            $new_academic->Semester()->create(['parity' => 1]);
            $this->info('New semestercreated: parity: 1');
            $new_academic->Semester()->create(['parity' => 2]);
            $this->info('New semestercreated: parity: 2');
        }
        $this->info('Academic semester command executed successfully');
    }
}
