<?php

namespace App\Console\Commands;

use App\OauthSession;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RemoveExpiredToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tokens:remove {table=all : Table from where you want to remove expired tokens. Possible values are, oauth_sessions, oauth_access_tokens and oauth_auth_codes. Leave empty for all.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete expired tokens from database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Pentru password_grant sterg si din oauth_session,
     * dar pentru auth_code sunt nevoit sa las session-ul ca sa nu-l mai intrebe de fiecare data de approve scopes
     *
     * @return mixed
     */
    public function handle()
    {
        $possible_tables = ['oauth_sessions', 'oauth_access_tokens', 'oauth_auth_codes', 'all'];
        if (!in_array($this->argument("table"), $possible_tables)) {
            $this->error('Table argument is not a valid table.');
            return;
        }
        if ($this->argument('table') == 'all') {
            $this->oauth_sessions();
            $this->oauth_access_tokens();
            $this->oauth_auth_codes();
        }else{
            call_user_func([$this, $possible_tables[array_search($this->argument('table'), $possible_tables)]]);
        }
        $this->info('Remove expired token command executed successfully');
    }

    /**
     * Sterg inregistrarile din oauth_sessions care au client cu password grant
     * si la care a expirat expire_time-ul din oauth_refresh_tokens
     * si care au fost facute de mai mult de 3 luni
     * Atentie! Client-ul s-ar putea sa aiba mai multe granturi si session-ul acesta sa nu fie din password grant.
     */
    private function oauth_sessions()
    {
        OauthSession::whereHas('OauthClient.OauthGrant', function ($query) {
            return $query->where('oauth_grants.id', 'password');
        })->join('oauth_access_tokens', 'oauth_access_tokens.session_id', '=', 'oauth_sessions.id')
            ->join('oauth_refresh_tokens', 'oauth_refresh_tokens.access_token_id', '=', 'oauth_access_tokens.id')
            ->where('oauth_refresh_tokens.expire_time', '<', time())
            ->where('oauth_access_tokens.expire_time', '<', time())
            ->where('oauth_sessions.created_at', '<', Carbon::now()->subMonth(3))
            ->delete();
    }

    /**
     * Sterg inregistrariile din oauth_auth_codes la care le-au expirat expire_time
     */
    private function oauth_auth_codes()
    {
        Db::table('oauth_auth_codes')->where('expire_time', '<', time())->delete();
    }

    /**
     * Sterg inregistrarile din oauth_access_tokens
     * la care a expirat expire_time-ul din oauth_refresh_tokens
     */
    private function oauth_access_tokens()
    {
        DB::table('oauth_access_tokens')
            ->where('oauth_access_tokens.expire_time', '<', time())
            ->join('oauth_refresh_tokens', 'oauth_refresh_tokens.access_token_id', '=', 'oauth_access_tokens.id')
            ->where('oauth_refresh_tokens.expire_time', '<', time())
            ->delete();
    }
}
