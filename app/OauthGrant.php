<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jedrzej\Pimpable\PimpableTrait;

class OauthGrant extends Model
{
    use PimpableTrait;

    /**
     * Determine what attributes can not be used for search in request.
     *
     */
    public $notSearchable = ['page', 'access_token'];

    /**
     * Allowed relations for with query
     * @return array
     */
    protected function getWithableRelations()
    {
        if (auth()->user()->hasRole(['admin'])) {
            return ['*'];
        }
        return [];
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id'];

    public function OauthClient()
    {
        return $this->belongsToMany(OauthClient::class, 'oauth_client_grants', 'grant_id', 'client_id');
    }
}
