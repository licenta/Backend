<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    use ModelTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'curriculums';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['section_id', 'first_semester_id', 'last_semester_id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function Section()
    {
        return $this->belongsTo(Section::class);
    }

    public function Subject()
    {
        return $this->hasMany(Subject::class);
    }

    public function SubjectSecondaryCurriculum()
    {
        return $this->hasMany(SubjectSecondaryCurriculum::class);
    }

    public function FirstSemester()
    {
        return $this->belongsTo(Semester::class, 'first_semester_id');
    }

    public function LastSemester()
    {
        return $this->belongsTo(Semester::class, 'last_semester_id');
    }

    public function Group()
    {
        return $this->hasMany(Group::class);
    }

    public function Structure()
    {
        return $this->hasMany(Structure::class);
    }
}
