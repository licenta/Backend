<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    use ModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function Section()
    {
        return $this->hasMany(Section::class);
    }

    public function Department()
    {
        return $this->hasMany(Department::class);
    }
}
