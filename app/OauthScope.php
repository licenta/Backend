<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jedrzej\Pimpable\PimpableTrait;

class OauthScope extends Model
{
    use PimpableTrait;

    /**
     * Determine what attributes can not be used for search in request.
     *
     */
    public $notSearchable = ['page', 'access_token'];

    /**
     * Allowed relations for with query
     * @return array
     */
    protected function getWithableRelations()
    {
        if (auth()->user()->hasRole(['admin'])) {
            return ['*'];
        }
        return [];
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'description'];

    public function OauthClient()
    {
        return $this->belongsToMany(OauthClient::class, 'oauth_client_scopes', 'scope_id', 'client_id');
    }

    public function OauthSession()
    {
        return $this->belongsToMany(OauthSession::class, 'oauth_session_scopes', 'scope_id', 'session_id');
    }
}
