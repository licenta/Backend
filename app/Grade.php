<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    use ModelTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['grade', 'feedback'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['upload_date'];

    public function Evaluation()
    {
        return $this->belongsTo(Evaluation::class);
    }

    public function Professor()
    {
        return $this->belongsTo(User::class, 'professor_user_id');
    }

    public function Student()
    {
        return $this->belongsTo(User::class, 'student_user_id');
    }
}
