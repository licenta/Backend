<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post('oauth/access_token', ['uses' => 'Auth\OAuthController@access_token'])->middleware('api');

Route::group(['prefix' => 'api/v1', 'middleware' => ['api']], function () {

    Route::group(['middleware' => ['valid_oauth_client_id', 'optional_oauth']], function () {

        Route::get('academic-year', ['as' => 'api.v1.academic-year.index', 'uses' => 'AcademicYearController@index']);
        Route::get('academic-year/{academic_year}/semester', ['as' => 'api.v1.academic-year.semester.index', 'uses' => 'SemesterController@index']);
        Route::get('activity-type', ['as' => 'api.v1.activity-type.index', 'uses' => 'ActivityTypeController@index']);
        Route::get('language', ['as' => 'api.v1.language.index', 'uses' => 'LanguageController@index']);
        Route::get('examination-type', ['as' => 'api.v1.examination-type.index', 'uses' => 'ExaminationTypeController@index']);
        Route::get('discipline-type', ['as' => 'api.v1.discipline-type.index', 'uses' => 'DisciplineTypeController@index']);
        Route::get('faculty', ['as' => 'api.v1.faculty.index', 'uses' => 'FacultyController@index']);
        Route::get('faculty/{faculty}/department', ['as' => 'api.v1.faculty.department.index', 'uses' => 'DepartmentController@index']);
        Route::get('department/{department}', ['as' => 'api.v1.department.index.find', 'uses' => 'DepartmentController@find']);
        Route::get('faculty/{faculty}/section', ['as' => 'api.v1.faculty.section.index', 'uses' => 'SectionController@index']);

    });

    Route::group(['middleware' => ['oauth']], function () {

        Route::group(['middleware' => ['oauth:basic_user_read']], function () {

            Route::get('user', ['as' => 'api.v1.user.index', 'uses' => 'UserController@index']);
            Route::get('me', ['as' => 'api.v1.me.index', 'uses' => 'UserController@me']);
            Route::get('user/profile-img/{user?}', ['as' => 'api.v1.user.profile-img.index', 'uses' => 'UserController@index_profile_img']);
        });

        Route::group(['middleware' => ['oauth:advanced_user_read']], function () {

            Route::get('curriculum/{curriculum}/structure', ['as' => 'api.v1.curriculum.structure.index', 'uses' => 'StructureController@index']);
            Route::get('curriculum/{curriculum}/group', ['as' => 'api.v1.curriculum.group.index', 'uses' => 'GroupController@index']);
            Route::get('section/{section}/curriculum', ['as' => 'api.v1.section.curriculum.index', 'uses' => 'CurriculumController@index']);
            Route::get('curriculum/{curriculum}/subject', ['as' => 'api.v1.curriculum.subject.index', 'uses' => 'SubjectController@index']);
            Route::get('subject/{subject}/subject-activity-type', ['as' => 'api.v1.subject.subject-activity-type.index', 'uses' => 'SubjectActivityTypeController@index']);
            Route::get('subject/{subject}/user', ['as' => 'api.v1.subject.user.index', 'uses' => 'SubjectController@subject_user']);
            Route::get('subject/{subject}/evaluation', ['as' => 'api.v1.subject.evaluation.index', 'uses' => 'EvaluationController@index']);
            Route::get('evaluation/{evaluation}/grade', ['as' => 'api.v1.evaluation.grade.index', 'uses' => 'GradeController@index']);
            Route::get('subject-activity-type/{subject_activity_type}/subject-group', ['as' => 'api.v1.subject-activity-type.subject-group.index', 'uses' => 'SubjectGroupController@index']);
            Route::get('room', ['as' => 'api.v1.room.index', 'uses' => 'RoomController@index']);

            Route::get('user/subject', ['as' => 'api.v1.user.subject.index', 'uses' => 'SubjectController@user_subject']);
            Route::get('user/subject-group', ['as' => 'api.v1.user.subject-group.index', 'uses' => 'SubjectGroupController@user_subject_group']);
            Route::get('user/group', ['as' => 'api.v1.user.group.index', 'uses' => 'GroupController@user_group']);
            Route::get('user/grade', ['as' => 'api.v1.user.grade.index', 'uses' => 'GradeController@user_grade']);
            Route::post('user/update/{user?}', ['as' => 'api.v1.user.update', 'uses' => 'UserController@update']);
        });

        Route::group(['middleware' => ['oauth:professor_write', 'role:teacher']], function () {

            Route::post('evaluation', ['as' => 'api.v1.evaluation.store', 'uses' => 'EvaluationController@store']);
            Route::put('evaluation/{evaluation}', ['as' => 'api.v1.evaluation.update', 'uses' => 'EvaluationController@update']);
            Route::delete('evaluation/{evaluation}', ['as' => 'api.v1.evaluation.destroy', 'uses' => 'EvaluationController@destroy']);

            Route::post('grade', ['as' => 'api.v1.grade.store', 'uses' => 'GradeController@store']);
            Route::put('grade/{grade}', ['as' => 'api.v1.grade.update', 'uses' => 'GradeController@update']);
            Route::delete('grade/{grade}', ['as' => 'api.v1.evaluation.destroy', 'uses' => 'GradeController@destroy']);
        });

        Route::group(['middleware' => ['oauth:admin_write', 'role:admin']], function () {

            Route::post('user', ['as' => 'api.v1.user.store', 'uses' => 'UserController@store']);
            Route::delete('user/{user}', ['as' => 'api.v1.user.destroy', 'uses' => 'UserController@destroy']);

            Route::post('role/{role}/user', ['as' => 'api.v1.role.store.user', 'uses' => 'RoleController@store_user']);
            Route::delete('role/{role}/user/{user}', ['as' => 'api.v1.role.delete.user', 'uses' => 'RoleController@destroy_user']);

            Route::get('role', ['as' => 'api.v1.role.index', 'uses' => 'RoleController@index']);

            Route::post('faculty', ['as' => 'api.v1.faculty.store', 'uses' => 'FacultyController@store']);
            Route::put('faculty/{faculty}', ['as' => 'api.v1.faculty.update', 'uses' => 'FacultyController@update']);
            Route::delete('faculty/{faculty}', ['as' => 'api.v1.faculty.destroy', 'uses' => 'FacultyController@destroy']);

            Route::post('department', ['as' => 'api.v1.department.store', 'uses' => 'DepartmentController@store']);
            Route::put('department/{department}', ['as' => 'api.v1.department.update', 'uses' => 'DepartmentController@update']);
            Route::delete('department/{department}', ['as' => 'api.v1.department.destroy', 'uses' => 'DepartmentController@destroy']);

            Route::post('section', ['as' => 'api.v1.section.store', 'uses' => 'SectionController@store']);
            Route::put('section/{section}', ['as' => 'api.v1.section.update', 'uses' => 'SectionController@update']);
            Route::delete('section/{section}', ['as' => 'api.v1.section.destroy', 'uses' => 'SectionController@destroy']);

            Route::post('language', ['as' => 'api.v1.language.store', 'uses' => 'LanguageController@store']);
            Route::put('language/{language}', ['as' => 'api.v1.language.update', 'uses' => 'LanguageController@update']);
            Route::delete('language/{language}', ['as' => 'api.v1.language.destroy', 'uses' => 'LanguageController@destroy']);

            Route::post('discipline-type', ['as' => 'api.v1.discipline-type.store', 'uses' => 'DisciplineTypeController@store']);
            Route::put('discipline-type/{discipline_type}', ['as' => 'api.v1.discipline-type.update', 'uses' => 'DisciplineTypeController@update']);
            Route::delete('discipline-type/{discipline_type}', ['as' => 'api.v1.discipline-type.destroy', 'uses' => 'DisciplineTypeController@destroy']);

            Route::post('examination-type', ['as' => 'api.v1.examination-type.store', 'uses' => 'ExaminationTypeController@store']);
            Route::put('examination-type/{examination_type}', ['as' => 'api.v1.examination-type.update', 'uses' => 'ExaminationTypeController@update']);
            Route::delete('examination-type/{examination_type}', ['as' => 'api.v1.examination-type.destroy', 'uses' => 'ExaminationTypeController@destroy']);

            Route::post('activity-type', ['as' => 'api.v1.activity-type.store', 'uses' => 'ActivityTypeController@store']);
            Route::put('activity-type/{activity_type}', ['as' => 'api.v1.activity-type.update', 'uses' => 'ActivityTypeController@update']);
            Route::delete('activity-type/{activity_type}', ['as' => 'api.v1.activity-type.destroy', 'uses' => 'ActivityTypeController@destroy']);

            Route::post('group', ['as' => 'api.v1.group.store', 'uses' => 'GroupController@store']);
            Route::put('group/{group}', ['as' => 'api.v1.group.update', 'uses' => 'GroupController@update']);
            Route::delete('group/{group}', ['as' => 'api.v1.group.destroy', 'uses' => 'GroupController@destroy']);

            Route::post('group/{group}/user', ['as' => 'api.v1.group.user.store', 'uses' => 'GroupController@store_user']);
            Route::delete('group/{group}/user/{user}', ['as' => 'api.v1.group.user.destroy', 'uses' => 'GroupController@destroy_user']);

            Route::post('curriculum', ['as' => 'api.v1.curriculum.store', 'uses' => 'CurriculumController@store']);
            Route::put('curriculum/{curriculum}', ['as' => 'api.v1.curriculum.update', 'uses' => 'CurriculumController@update']);
            Route::delete('curriculum/{curriculum}', ['as' => 'api.v1.curriculum.destroy', 'uses' => 'CurriculumController@destroy']);

            Route::post('structure', ['as' => 'api.v1.structure.store', 'uses' => 'StructureController@store']);
            Route::put('structure/{structure}', ['as' => 'api.v1.structure.update', 'uses' => 'StructureController@update']);
            Route::delete('structure/{structure}', ['as' => 'api.v1.structure.destroy', 'uses' => 'StructureController@destroy']);

            Route::post('subject', ['as' => 'api.v1.subject.store', 'uses' => 'SubjectController@store']);
            Route::put('subject/{subject}', ['as' => 'api.v1.subject.update', 'uses' => 'SubjectController@update']);
            Route::delete('subject/{subject}', ['as' => 'api.v1.subject.destroy', 'uses' => 'SubjectController@destroy']);

            Route::post('subject-activity-type', ['as' => 'api.v1.subject-activity-type.store', 'uses' => 'SubjectActivityTypeController@store']);
            Route::put('subject-activity-type/{subject_activity_type}', ['as' => 'api.v1.subject-activity-type.update', 'uses' => 'SubjectActivityTypeController@update']);
            Route::delete('subject-activity-type/{subject_activity_type}', ['as' => 'api.v1.subject-activity-type.destroy', 'uses' => 'SubjectActivityTypeController@destroy']);

            Route::post('subject-group', ['as' => 'api.v1.subject-group.store', 'uses' => 'SubjectGroupController@store']);
            Route::put('subject-group/{subject_group}', ['as' => 'api.v1.subject-group.update', 'uses' => 'SubjectGroupController@update']);
            Route::delete('subject-group/{subject_group}', ['as' => 'api.v1.subject-group.destroy', 'uses' => 'SubjectGroupController@destroy']);

            Route::post('subject-group/{subject_group}/user', ['as' => 'api.v1.subject-group.user.store', 'uses' => 'SubjectGroupController@store_user']);
            Route::delete('subject-group/{subject_group}/user/{user}', ['as' => 'api.v1.subject-group.user.destroy', 'uses' => 'SubjectGroupController@destroy_user']);

            Route::post('room', ['as' => 'api.v1.room.store', 'uses' => 'RoomController@store']);
            Route::put('room/{room}', ['as' => 'api.v1.room.update', 'uses' => 'RoomController@update']);
            Route::delete('room/{room}', ['as' => 'api.v1.room.destroy', 'uses' => 'RoomController@destroy']);
        });

        //OAuth
        Route::group(['middleware' => ['oauth:oauth_manager']], function () {

            Route::group(['middleware' => ['role:admin']], function () {

                Route::post('oauth-client', ['as' => 'api.v1.oauth-client.store', 'uses' => 'OAuthClient\OAuthClientController@store']);

                Route::get('oauth-grant', ['as' => 'api.v1.oauth-grant.index', 'uses' => 'OAuthClient\OAuthClientGrantController@index']);
                Route::post('oauth-client-grant', ['as' => 'api.v1.oauth-client-grant.store', 'uses' => 'OAuthClient\OAuthClientGrantController@store']);
                Route::delete('oauth-client/{oauth_client}/oauth-grant/{oauth_grant}', ['as' => 'api.v1.oauth-client.oauth-grant.destroy', 'uses' => 'OAuthClient\OAuthClientGrantController@destroy']);

                Route::post('oauth-client-scope', ['as' => 'api.v1.oauth-client-scope.store', 'uses' => 'OAuthClient\OAuthClientScopeController@store']);
            });

            Route::get('oauth-client/profile-img/{oauth_client}', ['as' => 'api.v1.oauth-client.profile-img.index', 'uses' => 'OAuthClient\OAuthClientController@index_client_img']);

            //TODO as putea sa fac un middleware in care sa-mi verifice daca sunt admin sau daca detin aplicatia
            Route::get('oauth-scope', ['as' => 'api.v1.oauth-scope.index', 'uses' => 'OAuthClient\OAuthClientScopeController@index']);

            Route::post('oauth-client/{oauth_client}', ['as' => 'api.v1.oauth-client.update', 'uses' => 'OAuthClient\OAuthClientController@update']);
            Route::get('oauth-session', ['as' => 'api.v1.oauth-session.index', 'uses' => 'OAuthClient\OauthSessionController@index']);
            Route::delete('oauth-session/{oauth_client}', ['as' => 'api.v1.oauth-session.destroy', 'uses' => 'OAuthClient\OauthSessionController@destroy']);

            Route::get('oauth-client', ['as' => 'api.v1.oauth-client.index', 'uses' => 'OAuthClient\OAuthClientController@index']);
            Route::get('oauth-client/{oauth_client}/secret', ['as' => 'api.v1.oauth-client.secret.index', 'uses' => 'OAuthClient\OAuthClientController@secret_index']);
            Route::put('oauth-client/{oauth_client}/regenerate', ['as' => 'api.v1.oauth-client.regenerate', 'uses' => 'OAuthClient\OAuthClientController@regenerate']);
            Route::delete('oauth-client/{oauth_client}', ['as' => 'api.v1.oauth-client.destroy', 'uses' => 'OAuthClient\OAuthClientController@destroy']);

            Route::post('oauth-endpoint', ['as' => 'api.v1.oauth-endpoint.store', 'uses' => 'OAuthClient\OAuthEndpointController@store']);
            Route::put('oauth-endpoint/{oauth_endpoint}', ['as' => 'api.v1.oauth-endpoint.update', 'uses' => 'OAuthClient\OAuthEndpointController@update']);
            Route::delete('oauth-endpoint/{oauth_endpoint}', ['as' => 'api.v1.oauth-endpoint.destroy', 'uses' => 'OAuthClient\OAuthEndpointController@destroy']);

            Route::delete('oauth-client/{oauth_client}/oauth-scope/{oauth_scope}', ['as' => 'api.v1.oauth-client.oauth-scope.destroy', 'uses' => 'OAuthClient\OAuthClientScopeController@destroy']);
        });
    });
});

Route::group(['middleware' => ['master_oauth_client']], function () {

    Route::group(['middleware' => ['check-authorization-params', 'oauth']], function () {

        Route::get('oauth/authorize', ['as' => 'oauth.authorize.get', 'uses' => 'Auth\OAuthController@oauth_authorize_show_form']);
        Route::post('oauth/authorize', ['as' => 'oauth.authorize.post', 'uses' => 'Auth\OAuthController@oauth_authorize_post']);
    });

    Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
    Route::post('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\PasswordController@reset']);
});

Route::get('/', ['as' => 'welcome', 'uses' => 'HomeController@welcome']);
