<?php

namespace App\Http\Controllers;

use App\AcademicYear;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AcademicYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $academic_year = AcademicYear::pimp()->simplePaginate();
        return response()->json(['academic_year' => $academic_year]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*public function store(Requests\AcademicYearRequest $request)
    {
        $academic_year = AcademicYear::create($request->all());
        return response()->json(['academic_year' => $academic_year]);
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function update(Requests\AcademicYearRequest $request, $id)
    {
        $academic_year = AcademicYear::findOrFail($id);
        $academic_year->update($request->all());
        return response()->json(['academic_year' => $academic_year]);
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy(Request $request)
    {
        AcademicYear::findOrFail($request->academic_year)->delete();
        return response()->json(['message' => 'AcademicYear deleted successfully']);
    }*/
}
