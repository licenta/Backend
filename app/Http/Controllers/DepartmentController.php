<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Department;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $department = Department::where('faculty_id', $request->faculty)->pimp()->simplePaginate();
        return response()->json(['department' => $department]);
    }

    /**
     * Display all departments for a user
     *
     * @return \Illuminate\Http\Response
     */
    public function find(Request $request)
    {
        $department = Department::pimp()->findOrFail($request->department);
        return response()->json(['department' => $department]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\DepartmentRequest $request)
    {
        $department = new Department($request->all());
        $department->faculty_id = $request->faculty_id;
        $department->save();
        return response()->json(['department' => $department]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\DepartmentRequest $request, $id)
    {
        $department = Department::findOrFail($id);
        $department->update($request->all());
        return response()->json(['department' => $department]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Department::findOrFail($request->department)->delete();
        return response()->json(['message' => 'Department deleted successfully']);
    }
}
