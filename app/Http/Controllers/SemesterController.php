<?php

namespace App\Http\Controllers;

use App\Semester;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SemesterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $semester = Semester::pimp()->where('academic_year_id', $request->academic_year)->get();
        return response()->json(['semester' => $semester]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*public function store(Requests\SemesterRequest $request)
    {
        $semester = Semester::create($request->all());
        return response()->json(['semester' => $semester]);
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function update(Requests\SemesterRequest $request, $id)
    {
        $semester = Semester::findOrFail($id);
        $semester->update($request->all());
        return response()->json(['semester' => $semester]);
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy(Request $request)
    {
        Semester::findOrFail($request->semester)->delete();
        return response()->json(['message' => 'Semester deleted successfully']);
    }*/
}
