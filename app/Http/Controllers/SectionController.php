<?php

namespace App\Http\Controllers;

use App\Section;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $section = Section::pimp()->where('faculty_id', $request->faculty)->simplePaginate();
        return response()->json(['section' => $section]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\SectionRequest $request)
    {
        $section = new Section($request->all());
        $section->faculty_id = $request->faculty_id;
        $section->language_id = $request->language_id;
        $section->save();
        return response()->json(['section' => $section]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\SectionRequest $request, $id)
    {
        $section = Section::findOrFail($id);
        $section->update($request->all());
        return response()->json(['section' => $section]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Section::findOrFail($request->section)->delete();
        return response()->json(['message' => 'Section deleted successfully']);
    }
}
