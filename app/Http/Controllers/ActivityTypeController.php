<?php

namespace App\Http\Controllers;

use App\ActivityType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ActivityTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activity_type = ActivityType::all();
        return response()->json(['activity_type' => $activity_type]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ActivityTypeRequest $request)
    {
        $activity_type = ActivityType::create($request->all());
        return response()->json(['activity_type' => $activity_type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ActivityTypeRequest $request, $id)
    {
        $activity_type = ActivityType::findOrFail($id);
        $activity_type->update($request->all());
        return response()->json(['activity_type' => $activity_type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        ActivityType::findOrFail($request->activity_type)->delete();
        return response()->json(['message' => 'Activity type deleted successfully']);
    }
}
