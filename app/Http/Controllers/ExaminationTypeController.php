<?php

namespace App\Http\Controllers;

use App\ExaminationType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ExaminationTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $examination_type = ExaminationType::all();
        return response()->json(['examination_type' => $examination_type]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ExaminationTypeRequest $request)
    {
        $examination_type = ExaminationType::create($request->all());
        return response()->json(['examination_type' => $examination_type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ExaminationTypeRequest $request, $id)
    {
        $examination_type = ExaminationType::findOrFail($id);
        $examination_type->update($request->all());
        return response()->json(['examination_type' => $examination_type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        ExaminationType::findOrFail($request->examination_type)->delete();
        return response()->json(['message' => 'Examination type deleted successfully']);
    }
}
