<?php

namespace App\Http\Controllers;

use App\DisciplineType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DisciplineTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discipline_type = DisciplineType::all();
        return response()->json(['discipline_type' => $discipline_type]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\DisciplineTypeRequest $request)
    {
        $discipline_type = DisciplineType::create($request->all());
        return response()->json(['discipline_type' => $discipline_type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\DisciplineTypeRequest $request, $id)
    {
        $discipline_type = DisciplineType::findOrFail($id);
        $discipline_type->update($request->all());
        return response()->json(['discipline_type' => $discipline_type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DisciplineType::findOrFail($request->discipline_type)->delete();
        return response()->json(['message' => 'Discipline type deleted successfully']);
    }
}
