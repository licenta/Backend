<?php

namespace App\Http\Controllers\Auth;

use App\OauthSession;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class OAuthController extends Controller
{
    public function oauth_authorize_show_form()
    {
        $authParams = Authorizer::getAuthCodeRequestParams();

        $formParams = array_except($authParams, 'client');

        $formParams['client_id'] = $authParams['client']->getId();

        $formParams['scope'] = implode(config('oauth2.scope_delimiter'), array_map(function ($scope) {
            return $scope->getId();
        }, $authParams['scopes']));

        $check_session = OauthSession::where('client_id', $formParams['client_id'])
            ->where('owner_id', auth()->user()->id)
            ->with(['OauthScope' => function ($query) use ($formParams) {
                return $query->whereIn('oauth_scopes.id', explode(',', $formParams['scope']));
            }])->get();

        $max = 0;
        foreach ($check_session as $check) {
            if ($check->OauthScope->count() > $max) {
                $max = $check->OauthScope->count();
            }
        }
        if ($max >= count($formParams['scopes'])) {
            $auth_code = Authorizer::issueAuthCode('user', auth()->user()->id, $authParams);
            return response()->json(['redirect_uri' => $auth_code]);
        }
        return response()->json(['params' => $formParams, 'client' => $authParams['client']]);
        //return view('oauth.authorization-form', ['params' => $formParams, 'client' => $authParams['client']]);
    }

    public function oauth_authorize_post(Request $request)
    {
        $params = Authorizer::getAuthCodeRequestParams();
        $params['user_id'] = auth()->user()->id;
        $redirectUri = Authorizer::issueAuthCode('user', $params['user_id'], $params);
        return response()->json(['redirect_uri' => $redirectUri]);
        //return redirect($redirectUri);

        /*$redirectUri = '/';

        // If the user has allowed the client to access its data, redirect back to the client with an auth code.
        if ($request->has('approve')) {
            $redirectUri = Authorizer::issueAuthCode('user', $params['user_id'], $params);
        }

        // If the user has denied the client to access its data, redirect back to the client with an error message.
        if ($request->has('deny')) {
            $redirectUri = Authorizer::authCodeRequestDeniedRedirectUri();
        }
        return redirect($redirectUri);*/
    }

    public function oauth_password_verify($username, $password)
    {
        $credentials = [
            'email' => $username,
            'password' => $password,
        ];

        if (auth()->once($credentials)) {
            return auth()->user()->id;
        }

        return false;
    }

    public function access_token()
    {
        $access_token = Authorizer::issueAccessToken();
        $user = \App\OauthSession::join('oauth_access_tokens', 'oauth_access_tokens.session_id', '=', 'oauth_sessions.id')
            ->where('oauth_access_tokens.id', $access_token['access_token'])
            ->first()->User->load('entity', 'Role');
        $user->access_token = $access_token;
        return response()->json($user);
    }
}
