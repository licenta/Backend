<?php

namespace App\Http\Controllers;

use App\SubjectActivityType;
use App\SubjectGroup;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SubjectGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!auth()->user()->hasRole(['admin', 'teacher'])) {
            $this->authorize('user_subscribed_to_subject', SubjectActivityType::findOrFail($request->subject_activity_type)->Subject);
        }
        $subject_group = SubjectGroup::pimp()->where('subject_activity_type_id', $request->subject_activity_type)->simplePaginate();
        return response()->json(['subject_group' => $subject_group]);
    }

    /**
     * Get all SubjectGroup for a User by a semester_id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user_subject_group(Request $request)
    {
        if (auth()->user()->hasRole(['admin']) && $request->has('user_id')) {
            $subject_group = SubjectGroup::whereHas('SubjectActivityType.Subject', function ($query) use ($request) {
                return $query->where('semester_id', $request->semester_id);
            })->whereHas('Student', function ($query) use ($request) {
                return $query->where('id', $request->user_id);
            })->orWhereHas('Professor', function ($query) use ($request) {
                return $query->where('id', $request->user_id);
            })->pimp()->simplePaginate();
        } else {
            $subject_group = SubjectGroup::whereHas('SubjectActivityType.Subject', function ($query) use ($request) {
                return $query->where('semester_id', $request->semester_id);
            })->whereHas('Student', function ($query) {
                return $query->where('id', auth()->user()->id);
            })->orWhereHas('Professor', function ($query) {
                return $query->where('id', auth()->user()->id);
            })->pimp()->simplePaginate();
        }

        return response()->json(['subject_group' => $subject_group]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public
    function store(Requests\SubjectGroupRequest $request)
    {
        $subject_group = new SubjectGroup($request->all());
        $subject_group->subject_activity_type_id = $request->subject_activity_type_id;
        $subject_group->save();
        return response()->json(['subject_group' => $subject_group]);
    }

    /**
     * Chain a Student with a SubjectGroup
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public
    function store_user(Request $request, $id)
    {
        $subject_group = SubjectGroup::findOrFail($id);
        $user = User::findOrFail($request->user_id);

        if (!$user->hasRole(['student'])) {
            return response()->json(['error' => 'User must be student type.']);
        }

        $subject_group->Student()->detach($user->id);
        $subject_group->Student()->attach($user->id);
        return response()->json(['subject_group' => $subject_group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Requests\SubjectGroupRequest $request, $id)
    {
        $subject_group = SubjectGroup::findOrFail($id);
        $subject_group->update($request->all());
        return response()->json(['subject_group' => $subject_group]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(Request $request)
    {
        SubjectGroup::findOrFail($request->subject_group)->delete();
        return response()->json(['message' => 'Subject group deleted successfully']);
    }

    /**
     * Detach the Student from SubjectGroup.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy_user(Request $request, $subject_group, $user)
    {
        $subject_group = SubjectGroup::findOrFail($request->subject_group);
        $subject_group->Student()->detach($user);
        return response()->json(['message' => 'Student deleted successfully from subject group']);
    }
}
