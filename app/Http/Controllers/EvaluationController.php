<?php

namespace App\Http\Controllers;

use App\Evaluation;
use App\Subject;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

use App\Http\Requests;

class EvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('user_subscribed_to_subject', ($subject = Subject::findOrFail($request->subject)));
        $evaluation = Evaluation::where('subject_id', $subject->id)->pimp()->get();
        return response()->json(['evaluation' => $evaluation]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\EvaluationRequest $request)
    {
        $evaluation = new Evaluation($request->all());
        $evaluation->subject_id = $request->subject_id;
        $evaluation->save();
        return response()->json(['evaluation' => $evaluation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\EvaluationRequest $request, $id)
    {
        $evaluation = Evaluation::findOrFail($id);
        $evaluation->update($request->all());
        return response()->json(['evaluation' => $evaluation]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $evaluation = Evaluation::with('Subject')->findOrFail($request->evaluation);
        $this->authorize('user_subscribed_to_subject', $evaluation->Subject);
        $evaluation->delete();
        return response()->json(['message' => 'Evaluation deleted successfully']);
    }
}
