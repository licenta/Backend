<?php

namespace App\Http\Controllers;

use App\Curriculum;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CurriculumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $curriculum = Curriculum::pimp()->where('section_id', $request->section)->simplePaginate();
        return response()->json(['curriculum' => $curriculum]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CurriculumRequest $request)
    {
        $curriculum = Curriculum::create($request->all());
        return response()->json(['curriculum' => $curriculum]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\CurriculumRequest $request, $id)
    {
        $curriculum = Curriculum::findOrFail($id);
        $curriculum->update($request->all());
        return response()->json(['curriculum' => $curriculum]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Curriculum::findOrFail($request->curriculum)->delete();
        return response()->json(['message' => 'Curriculum deleted successfully']);
    }
}
