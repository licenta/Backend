<?php

namespace App\Http\Controllers;

use App\Professor;
use App\Student;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    /**
     * Display user img.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_profile_img($user = null)
    {
        $user = is_null($user) ? auth()->user()->id : $user;
        if (!is_numeric($user)) {
            abort(404);
        }
        $real_path = storage_path('app/profile_img/') . $user;
        if (file_exists($real_path)) {
            $profile_img = Storage::get('profile_img/' . $user);
            $content_type = image_type_to_mime_type(exif_imagetype($real_path));
            return response($profile_img, 200)->header('Content-Type', $content_type);
        }
        $profile_img = Storage::get('default_profile_img/' . $user % 10 . '.jpg');
        return response($profile_img, 200)->header('Content-Type', 'image/jpeg');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->hasRole(['admin'])){
            $users = User::pimp()->simplePaginate();
        }else{
            $users = User::where('entity_type', 'student')->orWhere('entity_type', 'professor')->pimp()->simplePaginate();
        }
        return response()->json(['users' => $users]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function me()
    {
        $user = User::pimp()->find(auth()->user()->id);
        return response()->json($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\UserStoreRequest $request)
    {
        $user = new User($request->all());
        $user->password = Hash::make($request->password);
        if ($request->type == 'professor') {
            $professor = Professor::create($request->only('professor_type', 'department_id'));
            $user->entity_type = 'professor';
            $user->entity_id = $professor->id;
        } elseif ($request->type == 'student') {
            $student = Student::create($request->only('registration_number'));
            $user->entity_type = 'student';
            $user->entity_id = $student->id;
        }
        $user->save();
        $user->Role()->attach($request->role_id);
        if ($request->hasFile('upload') && $request->file('upload')->isValid()) {
            $request->file('upload')->move(storage_path('app/profile_img'), $user->id);
        }
        return response()->json(['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UserUpdateRequest $request, $id = null)
    {
        $user = auth()->user();
        if($id != null && $user->hasRole(['admin'])){
            $user = User::findOrFail($id);
        }
        $user->fill($request->all());
        $user->password = $request->has('password') ? Hash::make($request->password) : $user->password;
        $user->save();
        if ($request->hasFile('upload') && $request->file('upload')->isValid()) {
            $request->file('upload')->move(storage_path('app/profile_img'), $user->id);
        }
        return response()->json(['user' => $user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = User::findOrFail($request->user);
        if ($user->entity != null) {
            $user->entity->delete();
        }
        $real_path = storage_path('app/profile_img/') . $user->id;
        if (file_exists($real_path)) {
            Storage::delete('profile_img/' . $user->id);
        }
        $user->delete();
        return response()->json(['message' => 'User deleted successfully']);
    }
}
