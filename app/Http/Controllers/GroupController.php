<?php

namespace App\Http\Controllers;

use App\Curriculum;
use App\Group;
use App\Student;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!auth()->user()->hasRole(['admin', 'teacher'])) {
            $this->authorize('user_subscribed_to_curriculum_from_groups', Curriculum::findOrFail($request->curriculum));
        }
        $group = Group::pimp()->where('curriculum_id', $request->curriculum)->simplePaginate();
        return response()->json(['group' => $group]);
    }

    /**
     * Display all subjects subscribed by a user
     *
     * @return \Illuminate\Http\Response
     */
    public function user_group(Request $request)
    {
        $user_id = $request->has('user_id') ? $request->user_id : auth()->user()->id;
        $group = Group::whereHas('User', function($query) use ($user_id){
            return $query->where('id', $user_id);
        })->pimp()->get();
        return response()->json(['group' => $group]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\GroupRequest $request)
    {
        $group = new Group($request->all());
        $group->academic_year_id = $request->academic_year_id;
        $group->curriculum_id = $request->curriculum_id;
        $group->save();
        return response()->json(['group' => $group]);
    }

    /**
     * Add student to group
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store_user(Request $request)
    {
        $group = Group::findOrFail($request->group);
        $user = User::findOrFail($request->user_id);

        if (!$user->hasRole(['student'])) {
            return response()->json(['error' => 'User must be student type.']);
        }

        $group->User()->detach($request->user_id);
        $group->User()->attach($request->user_id);
        return response()->json(['group' => $group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\GroupRequest $request, $id)
    {
        $group = Group::findOrFail($id);
        $group->update($request->all());
        return response()->json(['group' => $group]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Group::findOrFail($request->group)->delete();
        return response()->json(['message' => 'Group deleted successfully']);
    }

    /**
     * Remove student from group
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy_user(Request $request)
    {
        $group = Group::findOrFail($request->group);
        $group->User()->detach($request->user);
        return response()->json(['message' => 'User deleted successfully from group']);
    }
}
