<?php

namespace App\Http\Controllers;

use App\AcademicYear;
use App\Structure;
use Illuminate\Http\Request;

use App\Http\Requests;

class StructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $structure = Structure::pimp()->where('curriculum_id', $request->curriculum)->simplePaginate();
        return response()->json(['structure' => $structure]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StructureRequest $request)
    {
        $structure = new Structure($request->all());
        $structure->semester_id = $request->semester_id;
        $structure->curriculum_id = $request->curriculum_id;
        $structure->save();
        return response()->json(['structure' => $structure]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\StructureRequest $request, $id)
    {
        $structure = Structure::findOrFail($id);
        $structure->update($request->all());
        return response()->json(['structure' => $structure]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Structure::findOrFail($request->structure)->delete();
        return response()->json(['message' => 'Structure deleted successfully']);
    }
}
