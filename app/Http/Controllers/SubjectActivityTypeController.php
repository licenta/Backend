<?php

namespace App\Http\Controllers;

use App\Subject;
use App\SubjectActivityType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SubjectActivityTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!auth()->user()->hasRole(['admin', 'teacher'])) {
            $this->authorize('user_subscribed_to_subject', Subject::findOrFail($request->subject));
        }
        $subject_activity_type = SubjectActivityType::pimp()->where('subject_id', $request->subject)->simplePaginate();
        return response()->json(['subject_activity_type' => $subject_activity_type]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\SubjectActivityTypeRequest $request)
    {
        $subject_activity_type = new SubjectActivityType($request->all());
        $subject_activity_type->subject_id = $request->subject_id;
        $subject_activity_type->save();
        return response()->json(['subject_activity_type' => $subject_activity_type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\SubjectActivityTypeRequest $request, $id)
    {
        $subject_activity_type = SubjectActivityType::findOrFail($id);
        $subject_activity_type->update($request->all());
        return response()->json(['subject_activity_type' => $subject_activity_type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        SubjectActivityType::findOrFail($request->subject_activity_type)->delete();
        return response()->json(['message' => 'Subject activity type deleted successfully']);
    }
}
