<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::pimp()->get();
        return response()->json(['role' => $role]);
    }

    /**
     * Store a newly created resource relation table.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store_user(Request $request)
    {
        $user = User::with(['Role' => function ($query) use ($request) {
            return $query->where('id', $request->role);
        }])->findOrFail($request->user_id);
        //TODO aici ar trebui sa pun un validate si sa returnez json on error
        $role = Role::findOrFail($request->role);

        if ($user->Role->count() != 0) {
            return response()->json(['error' => "User is already assigned to this role."]);
        }
        $user->Role()->attach($request->role);
        $user->load('Role');
        return response()->json(['user' => $user]);
    }

    /**
     * Remove the specified resource from the relation table.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_user(Request $request)
    {
        $user = User::findOrFail($request->user);
        $role = Role::findOrFail($request->role);
        if ($role->name == 'admin') {
            if ($role->User()->count() == 1) {
                return response()->json(['error' => 'You are the last administrator, you can\'t delete yourself.']);
            }
        }
        $user->Role()->detach($role->id);
        return response()->json(['message' => 'User role deleted successfully']);
    }
}
