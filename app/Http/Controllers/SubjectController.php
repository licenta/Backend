<?php

namespace App\Http\Controllers;

use App\Curriculum;
use App\Subject;
use App\SubjectActivityType;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!auth()->user()->hasRole(['admin', 'teacher'])) {
            $this->authorize('user_subscribed_to_curriculum_from_subjects', Curriculum::findOrFail($request->curriculum));
        }
        $subject = Subject::pimp()->where('curriculum_id', $request->curriculum)->simplePaginate();
        return response()->json(['subject' => $subject]);
    }

    /**
     * Display all subjects subscribed by a user
     *
     * @return \Illuminate\Http\Response
     */
    public function user_subject(Request $request)
    {
        if (auth()->user()->hasRole(['admin']) && $request->has('user_id')) {
            $subject = User::findOrFail($request->user_id)->Subject();
        } else {
            $subject = auth()->user()->Subject();
        }
        return response()->json(['subject' => $subject]);
    }

    /**
     * Display all users subscribed to a subject
     *
     * @return \Illuminate\Http\Response
     */
    public function subject_user(Request $request)
    {
        //$users = Subject::findOrFail($request->subject)->load('SubjectActivityType.SubjectGroup.Student');
        $users = SubjectActivityType::where('subject_id', $request->subject)->with(['SubjectGroup.Student' => function($query){
            return $query->groupBy('id')->pimp()->distinct();
        }])->get()->pluck('SubjectGroup.*.Student')->flatten();
        return response()->json(['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\SubjectRequest $request)
    {
        $subject = new Subject($request->all());
        $subject->semester_id = $request->semester_id;
        $subject->curriculum_id = $request->curriculum_id;
        $subject->save();
        return response()->json(['subject' => $subject]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\SubjectRequest $request, $id)
    {
        $subject = Subject::findOrFail($id);
        $subject->update($request->all());
        return response()->json(['subject' => $subject]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Subject::findOrFail($request->subject)->delete();
        return response()->json(['message' => 'Subject deleted successfully']);
    }
}
