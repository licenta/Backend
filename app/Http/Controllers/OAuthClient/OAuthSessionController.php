<?php

namespace App\Http\Controllers\OAuthClient;

use App\OauthClient;
use App\OauthSession;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

class OAuthSessionController extends Controller
{
    public function index()
    {
        $oauth_client = OauthClient::pimp()->whereHas('OauthSession', function ($query) {
            return $query->where('owner_id', auth()->user()->id);
        })->simplePaginate();
        return response()->json(['oauth_client' => $oauth_client]);
    }

    public function destroy(Request $request)
    {
        OauthSession::where('client_id', $request->oauth_client)->where('owner_id', auth()->user()->id)->delete();
        return response()->json(['message' => 'OAuth session deleted successfully']);
    }
}
