<?php

namespace App\Http\Controllers\OAuthClient;

use App\OAuthClient;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;

class OAuthClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasRole(['admin'])) {
            $oauth_client = OAuthClient::pimp()->simplePaginate();
        } else {
            $oauth_client = OAuthClient::pimp()->where('user_id', auth()->user()->id)->simplePaginate();
        }
        return response()->json(['oauth_client' => $oauth_client]);
    }

    /**
     * Display client img.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_client_img($oauth_client)
    {
        $real_path = storage_path('app/client_img/') . $oauth_client;
        if (file_exists($real_path)) {
            $client_img = Storage::get('client_img/' . $oauth_client);
            $content_type = image_type_to_mime_type(exif_imagetype($real_path));
            return response($client_img, 200)->header('Content-Type', $content_type);
        }
        $client_img = Storage::get('default_client_img/1.svg');
        return response($client_img, 200)->header('Content-Type', 'svg/xml');
    }

    /**
     * Display client_secret for a client.
     *
     * @return \Illuminate\Http\Response
     */
    public function secret_index(Request $request)
    {
        $oauth_client = OAuthClient::findOrFail($request->oauth_client);
        $this->authorize('user_owns_oauth_client', $oauth_client);
        $oauth_client->setHidden([]);
        return response()->json(['oauth_client' => $oauth_client]);
    }

    /**
     * Regenerate client id and secret.
     *
     * @return \Illuminate\Http\Response
     */
    public function regenerate(Request $request)
    {
        $oauth_client = OAuthClient::findOrFail($request->oauth_client);
        $this->authorize('user_owns_oauth_client', $oauth_client);
        $old_id = $oauth_client->id;
        $oauth_client->createTokens(10);
        $oauth_client->id = $old_id;
        $oauth_client->save();
        return response()->json(['oauth_client' => $oauth_client]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\OauthClientStoreRequest $request)
    {
        $oauth_client = new OAuthClient($request->all());
        $oauth_client->createTokens(10);
        $oauth_client->user_id = $request->user_id;
        $oauth_client->save();

        // Put role 'api_owner' to user who own this new client
        $api_owner = Role::where('name', 'api_owner')->first();
        $api_owner->User()->detach($request->user_id);
        $api_owner->User()->attach($request->user_id);

        if ($request->hasFile('upload') && $request->file('upload')->isValid()) {
            $request->file('upload')->move(storage_path('app/client_img'), $oauth_client->id);
        }
        return response()->json(['oauth_client' => $oauth_client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\OauthClientUpdateRequest $request, $id)
    {
        $oauth_client = OAuthClient::findOrFail($id);
        $oauth_client->update($request->all());
        if ($request->hasFile('upload') && $request->file('upload')->isValid()) {
            $request->file('upload')->move(storage_path('app/client_img'), $oauth_client->id);
        }
        return response()->json(['oauth_client' => $oauth_client]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $oauth_client = OAuthClient::findOrFail($request->oauth_client);
        if (!auth()->user()->hasRole(['admin'])) {
            $this->authorize('user_owns_oauth_client', $oauth_client);
        }
        $real_path = storage_path('app/client_img/') . $oauth_client->id;
        if (file_exists($real_path)) {
            Storage::delete('client_img/' . $oauth_client->id);
        }
        $oauth_client->delete();
        return response()->json(['message' => 'OAuth client deleted successfully']);
    }
}
