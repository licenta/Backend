<?php

namespace App\Http\Controllers\OAuthClient;

use App\OauthClientEndpoint;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class OAuthEndpointController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\OauthEndpointStoreRequest $request)
    {
        $oauth_endpoint = new OauthClientEndpoint($request->all());
        $oauth_endpoint->client_id = $request->client_id;
        $oauth_endpoint->save();
        return response()->json(['oauth_client_endpoint' => $oauth_endpoint]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\OauthEndpointUpdateRequest $request, $id)
    {
        $oauth_endpoint = OauthClientEndpoint::findOrFail($id);
        $oauth_endpoint->update($request->all());
        return response()->json(['oauth_client_endpoint' => $oauth_endpoint]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $endpoint = OauthClientEndpoint::findOrFail($request->oauth_endpoint);
        $this->authorize('user_owns_oauth_client', $endpoint->OauthClient);
        $endpoint->delete();
        return response()->json(['message' => 'OAuth client endpoint deleted successfully']);
    }
}
