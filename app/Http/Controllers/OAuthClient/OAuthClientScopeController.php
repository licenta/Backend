<?php

namespace App\Http\Controllers\OAuthClient;

use App\OauthClient;
use App\OauthScope;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class OAuthClientScopeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oauth_scope = OauthScope::pimp()->simplePaginate();
        return response()->json(['oauth_scope' => $oauth_scope]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\OauthClientScopeRequest $request)
    {
        $client = OauthClient::findOrFail($request->client_id);
        $client->OauthScope()->detach($request->scope_id);
        $client->OauthScope()->attach($request->scope_id);
        $client->load('OauthScope');
        return response()->json(['oauth_client_scope' => $client]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $oauth_client = OauthClient::findOrFail($request->oauth_client);
        if (!auth()->user()->hasRole(['admin'])) {
            $this->authorize('user_owns_oauth_client', $oauth_client);
        }
        $oauth_client->OauthScope()->detach($request->oauth_scope);
        return response()->json(['message' => 'OAuth client scope deleted successfully']);
    }
}
