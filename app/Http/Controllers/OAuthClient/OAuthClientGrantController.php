<?php

namespace App\Http\Controllers\OAuthClient;

use App\OauthGrant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\OauthClient;

class OAuthClientGrantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oauth_grant = OauthGrant::pimp()->get();
        return response()->json(['oauth_grant' => $oauth_grant]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\OauthClientGrantRequest $request)
    {
        $client = OauthClient::findOrFail($request->client_id);
        $client->OauthGrant()->detach($request->grant_id);
        $client->OauthGrant()->attach($request->grant_id);
        $client->load('OauthGrant');
        return response()->json(['oauth_client_grant' => $client]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        /*$oauth_client = OauthClient::findOrFail($request->oauth_client);
        $oauth_client->OauthGrant()->detach($request->oauth_grant);*/
        DB::table('oauth_client_grants')->where('client_id', $request->oauth_client)->where('grant_id', $request->oauth_grant)->delete();
        return response()->json(['message' => 'OAuth client grant deleted successfully']);
    }
}
