<?php

namespace App\Http\Controllers;

use App\Evaluation;
use App\Grade;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!auth()->user()->hasRole(['admin'])) {
            $this->authorize('user_subscribed_to_subject', Evaluation::findOrFail($request->evaluation)->Subject);
        }
        $grades = Grade::where('evaluation_id', $request->evaluation)->pimp()->simplePaginate();
        return response()->json(['grade' => $grades]);
    }

    /**
     * Display all subjects subscribed by a user
     *
     * @return \Illuminate\Http\Response
     */
    public function user_grade(Request $request)
    {
        if (auth()->user()->hasRole(['admin']) && $request->has('user_id')) {
            $user_id = $request->user_id;
        } else {
            $user_id = auth()->user()->id;
        }
        $grade = Grade::where('student_user_id', $user_id)->pimp()->simplePaginate();
        return response()->json(['grade' => $grade]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\GradeRequest $request)
    {
        $grade = new Grade($request->all());
        $grade->student_user_id = $request->user_id;
        $grade->professor_user_id = auth()->user()->id;
        $grade->evaluation_id = $request->evaluation_id;
        $grade->save();
        return response()->json(['grade' => $grade]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\GradeRequest $request, $id)
    {
        $grade = Grade::findOrFail($id);
        $grade->update($request->all());
        return response()->json(['grade' => $grade]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $grade = Grade::with('Evaluation.Subject')->findOrFail($request->grade);
        $this->authorize('user_subscribed_to_subject', $grade->Evaluation->Subject);
        $grade->delete();
        return response()->json(['message' => 'Grade deleted successfully']);
    }
}
