<?php

namespace App\Http\Requests;

use App\Evaluation;
use App\Http\Requests\Request;
use App\Subject;
use Illuminate\Support\Facades\Gate;

class EvaluationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->evaluation === null) {
            // this is for POST request
            $subject = Subject::findOrFail($this->subject_id);
        } else {
            // this is for PUT request
            $subject = Evaluation::findOrFail($this->evaluation)->Subject;
        }
        return Gate::allows('user_subscribed_to_subject', $subject);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'name' => 'required|string|min:2|max:70',
                    'code' => 'sometimes|required|integer',
                    'uploadable_solution' => 'required|boolean',
                    'start_date' => 'required|date|before:end_date',
                    'end_date' => 'required|date|after:start_date|after:now',
                    'subject_id' => 'required|integer|exists:subjects,id',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'name' => 'sometimes|required|string|min:2|max:70',
                    'code' => 'sometimes|sometimes|required|integer',
                    'uploadable_solution' => 'sometimes|required|boolean',
                    'start_date' => 'sometimes|required_with:end_date|date|before:end_date',
                    'end_date' => 'sometimes|required_with:start_date|date|after:start_date|after:now',
                ];
            }
            default:
                break;
        }
    }
}
