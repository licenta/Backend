<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;

class SubjectGroupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(request()->has('user_id')){
            $user = User::findOrFail($this->user_id);
            if(! $user->hasRole(['teacher'])){
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name' => 'required|string|max:70',
                    'user_id' => 'required|integer|exists:users,id',
                    'room_id' => 'sometimes|required|integer|exists:rooms,id',
                    'subject_activity_type_id' => 'required|integer|exists:subject_activity_types,id',
                    'hour' => 'required|date_format:H:i'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name' => 'sometimes|required|string|max:70',
                    'user_id' => 'sometimes|required|integer|exists:users,id',
                    'room_id' => 'sometimes|required|integer|exists:rooms,id',
                    'hour' => 'sometimes|required|date_format:H:i'
                ];
            }
            default:break;
        }
    }
}
