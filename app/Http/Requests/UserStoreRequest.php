<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:2|max:70',
            'password' => 'required|string|min:6',
            'email' => 'required|email|max:255|unique:users',
            'first_name' => 'required|string|min:2|max:70',
            'last_name' => 'required|string|min:2|max:70',
            'type' => 'required|in:admin,professor,student',
            'role_id' => 'required|integer|exists:roles,id',
            'upload' => 'sometimes|required|image|max:2000',
            'professor_type' => 'required_if:type,professor|string|in:asistent,doctorand,lector,profesor universitar',
            'department_id' => 'required_if:type,professor|integer|exists:departments,id',
            'registration_number' => 'required_if:type,student|integer',
        ];
    }
}
