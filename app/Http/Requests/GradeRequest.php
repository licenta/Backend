<?php

namespace App\Http\Requests;

use App\Evaluation;
use App\Grade;
use App\Http\Requests\Request;
use App\User;
use Illuminate\Support\Facades\Gate;

class GradeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->method() == 'POST') {
            $subject = Evaluation::findOrFail($this->evaluation_id)->Subject;
            $user = User::findOrFail($this->user_id);
            if(! $user->hasRole(['student'])){
                return false;
            }
            // verific daca teacher-ul si student-ul apartin de subject-ul respectiv
            return Gate::allows('user_subscribed_to_subject', $subject) && Gate::forUser($user)->allows('user_subscribed_to_subject', $subject);
        }
        if ($this->method() == 'PUT' || $this->method() == 'PATCH') {
            $subject = Grade::findOrFail($this->route()->grade)->Evaluation->Subject;
            return Gate::allows('user_subscribed_to_subject', $subject);
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'grade' => 'required|numeric|min:1|max:10',
                    'feedback' => 'sometimes|required|string',
                    'user_id' => 'required|integer|exists:users,id',
                    'evaluation_id' => 'required|integer|exists:evaluations,id',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'grade' => 'sometimes|required|numeric|min:1|max:10',
                    'feedback' => 'sometimes|required|string',
                ];
            }
            default:
                break;
        }
    }
}
