<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use App\OauthClient;

class OauthEndpointStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('user_owns_oauth_client', OauthClient::findOrFail($this->client_id));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'redirect_uri' => 'required|url',
            'client_id' => 'required|integer|exists:oauth_clients,id'
        ];
    }
}
