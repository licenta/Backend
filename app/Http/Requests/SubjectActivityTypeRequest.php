<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SubjectActivityTypeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'hour_count' => 'required|integer|max:127',
                    'subject_id' => 'required|integer|exists:subjects,id',
                    'activity_type_id' => 'required|integer|exists:activity_types,id',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'hour_count' => 'sometimes|required|integer|max:127',
                    'activity_type_id' => 'sometimes|required|integer|exists:activity_types,id',
                ];
            }
            default:break;
        }
    }
}
