<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\OauthClient;
use Illuminate\Support\Facades\Gate;

class OauthClientUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('user_owns_oauth_client', OauthClient::findOrFail(request()->oauth_client));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|sometimes|string|min:3|max:70',
            'upload' => 'sometimes|required|image|max:2000',
        ];
    }
}
