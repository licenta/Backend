<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OauthClientStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:70',
            'user_id' => 'required|integer|exists:users,id',
            'upload' => 'sometimes|required|image|max:2000',
        ];
    }
}
