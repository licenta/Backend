<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\OauthClientEndpoint;
use Illuminate\Support\Facades\Gate;

class OauthEndpointUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('user_owns_oauth_client', OauthClientEndpoint::findOrFail(request()->oauth_endpoint)->OauthClient);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'redirect_uri' => 'required|sometimes|url'
        ];
    }
}
