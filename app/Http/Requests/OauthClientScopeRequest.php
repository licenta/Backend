<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OauthClientScopeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'client_id' => 'required|integer|exists:oauth_clients,id',
                    'scope_id' => 'required|string|exists:oauth_scopes,id'
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'client_id' => 'sometimes|required|integer|exists:oauth_clients,id',
                    'scope_id' => 'sometimes|required|string|exists:oauth_scopes,id'
                ];
            }
            default:
                break;
        }
    }
}
