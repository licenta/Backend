<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CurriculumRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'section_id' => 'required|integer|exists:sections,id',
                    'first_semester_id' => 'required|integer|exists:semesters,id',
                    'last_semester_id' => 'required|integer|exists:semesters,id',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'section_id' => 'sometimes|required|integer|exists:sections,id',
                    'first_semester_id' => 'sometimes|required|integer|exists:semesters,id',
                    'last_semester_id' => 'sometimes|required|integer|exists:semesters,id',
                ];
            }
            default:
                break;
        }
    }
}
