<?php

namespace App\Http\Requests;

use App\AcademicYear;
use App\Http\Requests\Request;
use App\Semester;
use Carbon\Carbon;

class StructureRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                $academic_year = AcademicYear::whereHas('Semester', function ($query) {
                    return $query->where('id', $this->semester_id);
                })->first();
                $start_year = Carbon::createFromDate($academic_year->start_year, 1, 1);
                $end_year = Carbon::createFromDate($academic_year->end_year, 12, 31);
                return [
                    'description' => 'required|string|min:2',
                    'from_date' => 'required|date_format:Y-m-d|before:to_date|after:' . $start_year,
                    'to_date' => 'required|date_format:Y-m-d|after:from_date|before:' . $end_year,
                    'semester_id' => 'required|integer|exists:semesters,id',
                    'curriculum_id' => 'required|integer|exists:curriculums,id',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                $academic_year = AcademicYear::whereHas('Semester.Structure', function ($query) {
                    return $query->where('id', $this->route()->structure);
                })->first();
                $start_year = Carbon::createFromDate($academic_year->start_year, 1, 1);
                $end_year = Carbon::createFromDate($academic_year->end_year, 12, 31);
                return [
                    'description' => 'sometimes|required|string|min:2',
                    'from_date' => 'sometimes|required_with:to_date|date_format:Y-m-d|before:to_date|after:' . $start_year,
                    'to_date' => 'sometimes|required_with:from_date|date_format:Y-m-d|after:from_date|before:' . $end_year,
                ];
            }
            default:
                break;
        }
    }
}
