<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SubjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name' => 'required|string|max:70',
                    'semester_id' => 'required|integer|exists:semesters,id',
                    'curriculum_id' => 'required|integer|exists:curriculums,id',
                    'discipline_type_id' => 'required|integer|exists:discipline_types,id',
                    'examination_type_id' => 'required|integer|exists:examination_types,id',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name' => 'sometimes|required|string|max:70',
                    //'semester_id' => 'sometimes|required|integer|exists:semesters,id',
                    //'curriculum_id' => 'sometimes|required|integer|exists:curriculums,id',
                    'discipline_type_id' => 'sometimes|required|integer|exists:discipline_types,id',
                    'examination_type_id' => 'sometimes|required|integer|exists:examination_types,id',
                ];
            }
            default:break;
        }
    }
}
