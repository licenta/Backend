<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SemesterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'parity' => 'required|integer|min:1|max:2',
                    'academic_year_id' => 'required|integer|exists:academic_years,id',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'parity' => 'sometimes|required|integer|min:1|max:2',
                ];
            }
            default:
                break;
        }
    }
}
