<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DepartmentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'name' => 'required|string|max:70|unique:departments',
                    'faculty_id' => 'required|integer|exists:faculties,id'
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'name' => 'sometimes|required|string|max:70|unique:departments'
                ];
            }
            default:
                break;
        }
    }
}
