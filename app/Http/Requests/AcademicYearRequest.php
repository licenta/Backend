<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AcademicYearRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'start_year' => 'required|integer|date_format:Y|unique:academic_year|max:' . $this->end_year,
                    'end_year' => 'required|integer|date_format:Y|unique:academic_year|min:' . $this->start_year,
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'start_year' => 'sometimes|required_with:end_year|integer|date_format:Y|unique:academic_year|max:' . $this->end_year,
                    'end_year' => 'sometimes|required_with:start_year|integer|date_format:Y|unique:academic_year|min:' . $this->start_year,
                ];
            }
            default:
                break;
        }
    }
}
