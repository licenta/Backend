<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|required|string|min:2|max:70',
            'email' => 'sometimes|required|email|max:255|unique:users',
            'first_name' => 'sometimes|required|string|min:2|max:70',
            'last_name' => 'sometimes|required|string|min:2|max:70',
            'upload' => 'sometimes|required|image|max:2000'
        ];
    }
}
