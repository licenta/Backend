<?php

namespace App\Http\Middleware;

use Closure;

class OptionalOAuthMiddleware extends \LucaDegasperi\OAuth2Server\Middleware\OAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $scopesString = null)
    {
        if (!$request->hasHeader('Authorization')) {
            return $next($request);
        }
        $scopes = [];

        if (!is_null($scopesString)) {
            $scopes = explode('+', $scopesString);
        }

        $this->authorizer->setRequest($request);

        $this->authorizer->validateAccessToken($this->httpHeadersOnly);
        $this->validateScopes($scopes);

        auth()->onceUsingId($this->authorizer->getResourceOwnerId());
        return $next($request);
    }
}
