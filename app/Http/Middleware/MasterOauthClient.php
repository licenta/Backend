<?php

namespace App\Http\Middleware;

use Closure;

class MasterOauthClient
{
    /**
     * Verify if request is made from master oauth client.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('OAUTH_MASTER_CLIENT_ID') != $request->master_client_id || env('OAUTH_MASTER_CLIENT_SECRET') != $request->master_client_secret) {
            return response('Unauthorized.', 401);
        }
        return $next($request);
    }
}
