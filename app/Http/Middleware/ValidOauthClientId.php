<?php

namespace App\Http\Middleware;

use App\OauthClient;
use Closure;

class ValidOauthClientId
{
    /**
     * Verify if request has a valid client_id.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $oauth_client = OauthClient::find($request->client_id);
        if ($oauth_client == null) {
            return response('Unauthorized.', 401);
        }
        return $next($request);
    }
}
