<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redis;
use MongoClient;
use Predis\Client;

class LogApi
{
    /**
     * Log api request in mongo
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $log_driver = env('LOG_DRIVER', '');
        if ($log_driver == 'mongo' || 'redis') {
            $log_array = ['endpoint' => $request->getPathInfo(),
                'method' => $request->getMethod(),
                'client_ip' => $request->getClientIp(),
                'user_id' => auth()->check() ? auth()->user()->id : null,
                'request' => $request->all(),
                'env' => env('APP_ENV')];
        }
        if ($log_driver == 'mongo') {
            $driver = config('database.connections.mongo.driver');
            $host = config('database.connections.mongo.host');
            $database = config('database.connections.mongo.database');
            $username = config('database.connections.mongo.username');
            $password = config('database.connections.mongo.password');
            $m = new MongoClient($driver + "://${username}:${password}@" + $host);
            $collection = $m->$database->api_endpoints;
            $collection->insert($log_array);
        } else if ($log_driver == 'redis') {
            $redis_host = config('database.redis.default.host');
            $redis_password = config('database.redis.default.password');
            $redis_port = config('database.redis.default.port');
            $predis = new Client("tcp://${redis_host}:${redis_port}");
            $predis->rpush('api_log', json_encode($log_array));
        }
    }
}
