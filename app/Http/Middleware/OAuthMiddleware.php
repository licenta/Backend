<?php

namespace App\Http\Middleware;

use Closure;

class OAuthMiddleware extends \LucaDegasperi\OAuth2Server\Middleware\OAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $scopesString = null)
    {
        //$route_name = $request->route()->getName();
        $scopes = [];

        if (!is_null($scopesString)) {
            $scopes = explode('+', $scopesString);
        }

        /*if (!is_null($route_name) && config('oauth2.limit_clients_to_scopes')) {
            $scopes[] = $route_name;
        }*/

        $this->authorizer->setRequest($request);

        $this->authorizer->validateAccessToken($this->httpHeadersOnly);
        $this->validateScopes($scopes);

        auth()->onceUsingId($this->authorizer->getResourceOwnerId());

        return $next($request);
    }
}
