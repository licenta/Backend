<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
    use ModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'from_date', 'to_date'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['from_date', 'to_date'];

    /**
     * Format from date attribute
     */
    public function getFromDateAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    /**
     * Format to date attribute
     */
    public function getToDateAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function Curriculum()
    {
        return $this->belongsTo(Curriculum::class);
    }
}
