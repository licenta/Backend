<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use ModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function Professor()
    {
        return $this->hasMany(Professor::class);
    }

    public function Faculty()
    {
        return $this->belongsTo(Faculty::class);
    }
}
