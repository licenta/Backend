<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OauthSession extends Model
{

    public function OauthClient()
    {
        return $this->belongsTo(OauthClient::class, 'client_id');
    }

    public function OauthScope()
    {
        return $this->belongsToMany(OauthScope::class, 'oauth_session_scopes', 'session_id', 'scope_id');
    }

    public function User(){
        return $this->belongsTo(User::class, 'owner_id');
    }
}
