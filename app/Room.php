<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use ModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'location', 'extra_info'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function GroupSubject()
    {
        return $this->hasMany(GroupSubject::class);
    }
}
