<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use ModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function Faculty()
    {
        return $this->belongsTo(Faculty::class);
    }

    public function Language()
    {
        return $this->belongsTo(Language::class);
    }

    public function Curriculum()
    {
        return $this->hasMany(Curriculum::class);
    }
}
