<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
    realpath(__DIR__ . '/../')
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

$app->configureMonologUsing(function ($monolog) {
    if (env('LOG_DRIVER', '') == 'mongo') {
        $driver = config('database.connections.mongo.driver');
        $host = config('database.connections.mongo.host');
        $database = config('database.connections.mongo.database');
        $username = config('database.connections.mongo.username');
        $password = config('database.connections.mongo.password');
        $monolog->pushHandler(new \Monolog\Handler\MongoDBHandler(new MongoDB\Driver\Manager($driver . "://${username}:${password}@" . $host), $database, "exceptions"));
    } else if (env('LOG_DRIVER', '') == 'redis') {
        $redis_host = config('database.redis.default.host');
        $redis_password = config('database.redis.default.password');
        $redis_port = config('database.redis.default.port');
        $redis = new \Monolog\Handler\RedisHandler(new Predis\Client("tcp://${redis_host}:${redis_port}"), "logs", "prod");
        $monolog->pushHandler($redis);
    } else {
        $monolog->pushHandler(new \Monolog\Handler\StreamHandler(storage_path('logs/laravel.log')));
    }
});

return $app;
